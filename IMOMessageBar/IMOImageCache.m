//
//  IMOImageCache.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/10/14.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "IMOImageCache.h"

@interface IMOImageCache ()

@property (nonatomic, strong) NSCache *memoryCache;

@end

@implementation IMOImageCache

- (void)storeImage:(UIImage *)image forKey:(NSString *)key {
    if (image && key) {
        [self.memoryCache setObject:image forKey:key cost:image.size.width * image.size.height * image.scale];
    }
}

- (UIImage *)queryImageWithKey:(NSString *)key {
    if (key) {
        return [self.memoryCache objectForKey:key];
    }
    return nil;
}

- (void)removeAllCaches {
    [self.memoryCache removeAllObjects];
}

- (NSCache *)memoryCache {
    if (!_memoryCache) {
        _memoryCache = [[NSCache alloc] init];
    }
    return _memoryCache;
}

@end

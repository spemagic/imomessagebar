//
//  IMOEmojiModel.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/10/13.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMOEmojiModel : NSObject

@property (nonatomic, assign) BOOL isGIF;

// 填充NSString对象
@property (nonatomic, copy) NSArray *emojiNames;

@end

//
//  IMOEmojiPickerViewModel.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/10/9.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXTERN const NSInteger kEmojiPickerEmojiRowsInPortrait;
FOUNDATION_EXTERN const NSInteger kEmojiPickerEmojiRowsInLandscape;
FOUNDATION_EXTERN const NSInteger kEmojiPickerEmojiColumnsInPortrait;
FOUNDATION_EXTERN const NSInteger kEmojiPickekEmojiColumnsInLandscape;

FOUNDATION_EXTERN const NSInteger kEmojiPickerGIFRowsInPortrait;
FOUNDATION_EXTERN const NSInteger kEmojiPickerGIFRowsInLandscape;
FOUNDATION_EXTERN const NSInteger kEmojiPickerGIFColumnsInPortrait;
FOUNDATION_EXTERN const NSInteger kEmojiPickekGIFColumnsInLandscape;

FOUNDATION_EXTERN NSString *const kEmojiPickerDeleteButtonKey;
FOUNDATION_EXTERN NSString *const kEmojiPickerEmptyButtonKey;

@interface IMOEmojiViewModel : NSObject

+ (NSInteger)numberOfEmojiRowsInPortrait;
+ (NSInteger)numberOfEmojiRowsInLandscape;
+ (NSInteger)numberOfEmojiColumnsInPortrait;
+ (NSInteger)numberOfEmojiColumnsInLandscape;

+ (NSInteger)numberOfGIFRowsInPortrait;
+ (NSInteger)numberOfGIFRowsInLandscape;
+ (NSInteger)numberOfGIFColumnsInPortrait;
+ (NSInteger)numberOfGIFColumnsInLandscape;

+ (NSArray *)getAllEmojiNamesWithPortraitMode:(BOOL)isPortrait;

@end

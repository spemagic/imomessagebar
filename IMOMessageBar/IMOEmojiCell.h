//
//  IMOEmojiCell.h
//  IMOMessageBar
//
//  Created by spemagic on 10/13/15.
//  Copyright © 2015 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMOEmojiCell : UICollectionViewCell

@property (nonatomic, readonly) UIImageView *emojiImageView;

- (void)fillCellWithImageName:(NSString *)name atIndexPath:(NSIndexPath *)indexPath;

@end

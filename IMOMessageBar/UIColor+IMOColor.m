//
//  UIColor+IMOColor.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/9/13.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "UIColor+IMOColor.h"
#import "IMOMacros.h"

@implementation UIColor (IMOColor)

+ (UIColor *)randomColor {
    CGFloat seed = 1000000;
    CGFloat r = arc4random_uniform(seed);
    CGFloat g = arc4random_uniform(seed);
    CGFloat b = arc4random_uniform(seed);
    return RGB(r/seed * 255.0, g/seed * 255.0, b/seed * 255.0);
}

+ (UIColor *)seperatorColor {
    return RGB(182, 182, 182);
}

+ (UIColor *)collectionViewBackgroundColor {
    return RGB(241, 241, 241);
}

+ (UIColor *)pageControlIndicatorColor {
    return [UIColor colorWithWhite:0.678 alpha:1];
}

+ (UIColor *)pageControlCurrentIndicatorColor {
    return [UIColor colorWithWhite:0.471 alpha:1];
}

@end

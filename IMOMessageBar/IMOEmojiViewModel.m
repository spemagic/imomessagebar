
//
//  IMOEmojiPickerViewModel.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/10/9.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "IMOEmojiViewModel.h"
#import "IMOMacros.h"
#import "IMOPageItem.h"
#import "IMOEmojiModel.h"

NSString *const kEmojiPickerDeleteButtonKey = @"delete_emoji_normal";
NSString *const kEmojiPickerEmptyButtonKey = @"emoji_picker_empty_button";

@implementation IMOEmojiViewModel

#pragma mark - Data prepare

+ (NSInteger)numberOfEmojiRowsInPortrait {
    return 3;
}

+ (NSInteger)numberOfEmojiRowsInLandscape {
    return 3;
}

+ (NSInteger)numberOfEmojiColumnsInPortrait {
    if (kIsiPhone6P) {
        return 9;
    } else if (kIsiPhone6) {
        return 8;
    } else {
        return 7;
    }
}

+ (NSInteger)numberOfEmojiColumnsInLandscape {
    if (kIsiPhone6PInLandscape) {
        return 12;
    } else if (kIsiPhone6InLandscape) {
        return 11;
    } else {
        return 10;
    }
}

+ (NSInteger)numberOfGIFRowsInPortrait {
    return 2;
}

+ (NSInteger)numberOfGIFRowsInLandscape {
    return 2;
}

+ (NSInteger)numberOfGIFColumnsInPortrait {
    if (kIsiPhone6P) {
        return 5;
    } else if (kIsiPhone6) {
        return 4;
    } else {
        return 4;
    }
}

+ (NSInteger)numberOfGIFColumnsInLandscape {
    if (kIsiPhone6PInLandscape) {
        return 7;
    } else if (kIsiPhone6InLandscape) {
        return 6;
    } else {
        return 6;
    }
}

+ (NSInteger)numberOfImagesPerPageInPortraitMode:(BOOL)isPortrait isGIF:(BOOL)isGIF {
    if (isPortrait) {
        return isGIF ? [IMOEmojiViewModel numberOfGIFRowsInPortrait] * [IMOEmojiViewModel numberOfGIFColumnsInPortrait] : [IMOEmojiViewModel numberOfEmojiRowsInPortrait] * [IMOEmojiViewModel numberOfEmojiColumnsInPortrait];
    } else {
        return isGIF ? [IMOEmojiViewModel numberOfGIFRowsInLandscape] * [IMOEmojiViewModel numberOfGIFColumnsInLandscape] : [IMOEmojiViewModel numberOfEmojiRowsInLandscape] * [IMOEmojiViewModel numberOfEmojiColumnsInLandscape];
    }
}

+ (NSArray *)getGIFNamesWithPortraitMode:(BOOL)isPortrait {
    NSMutableArray *gifsFromFile = [NSMutableArray array];
    for (int i = 1; i <= 11; i++) {
        NSString *gifName = [NSString stringWithFormat:@"bus%d@2x.png", (int)i];
        [gifsFromFile addObject:gifName];
    }
    
    NSInteger numberOfGIFsPerPage = [IMOEmojiViewModel numberOfImagesPerPageInPortraitMode:isPortrait isGIF:YES];
    
    NSMutableArray *gifs = [NSMutableArray array];
    NSMutableArray *removed = [NSMutableArray array];

    IMOEmojiModel *emojiModel = nil;
    while (gifsFromFile.count != 0) {
        NSString *firstObject = gifsFromFile.firstObject;
        [removed addObject:firstObject];
        
        if (removed.count == 1) {
            emojiModel = [[IMOEmojiModel alloc] init];
            emojiModel.isGIF = YES;
            [gifs addObject:emojiModel];
        } else if (removed.count == numberOfGIFsPerPage) {
            emojiModel.emojiNames = [removed copy];
            [removed removeAllObjects];
        }
        
        [gifsFromFile removeObject:firstObject];
        if (gifsFromFile.count == 0) {
            emojiModel.emojiNames = [removed copy];
        }
    }
    return gifs;
}

+ (NSArray *)getEmojiNamesWithPortraitMode:(BOOL)isPortrait {
    NSString *emotionPlistPath = [[NSBundle mainBundle] pathForResource:@"IMOEmoji" ofType:@"plist"];
    NSArray *emojisFromFile = [NSDictionary dictionaryWithContentsOfFile:emotionPlistPath][@"emojiImages"];
    NSMutableArray *originEmojis = [NSMutableArray arrayWithArray:emojisFromFile];
    
    NSUInteger numberOfEmojiPerPage = [IMOEmojiViewModel numberOfImagesPerPageInPortraitMode:isPortrait isGIF:NO];
    
    NSMutableArray *emojis = [NSMutableArray array];
    NSMutableArray *removed = [NSMutableArray array];
    
    IMOEmojiModel *emojiModel = nil;
    while (originEmojis.count != 0) {
        NSString *firstObject = originEmojis.firstObject;
        [removed addObject:firstObject];
        
        BOOL skip = NO;
        if (removed.count == 1) {
            emojiModel = [[IMOEmojiModel alloc] init];
            emojiModel.isGIF = NO;
            [emojis addObject:emojiModel];
        } else if (removed.count == numberOfEmojiPerPage) {
            [removed removeLastObject];
            [removed addObject:kEmojiPickerDeleteButtonKey];
            emojiModel.emojiNames = [removed copy];
            [removed removeAllObjects];
            skip = YES;
        }
        
        if (!skip) {
            [originEmojis removeObject:firstObject];
        }
        
        if (originEmojis.count == 0) {
            [removed addObject:kEmojiPickerDeleteButtonKey];
            emojiModel.emojiNames = [removed copy];
        }
    }
    return emojis;
}

+ (NSArray *)getAllEmojiNamesWithPortraitMode:(BOOL)isPortrait {
    NSArray *emojis = [IMOEmojiViewModel getEmojiNamesWithPortraitMode:isPortrait];
    NSArray *gifs = [IMOEmojiViewModel getGIFNamesWithPortraitMode:isPortrait];
    
    NSMutableArray *emojiNames = [NSMutableArray array];
    if (emojis.count) {
        [emojiNames addObject:emojis];
    }
    if (gifs.count) {
        [emojiNames addObject:gifs];
    }
    return emojiNames;
}

@end

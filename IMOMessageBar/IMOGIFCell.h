//
//  IMOGIFCell.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/9/28.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMOGIFCell : UICollectionViewCell

@property (nonatomic, readonly) UIImageView *emojiImageView;

- (void)fillCellWithImageName:(NSString *)name atIndexPath:(NSIndexPath *)indexPath;

@end


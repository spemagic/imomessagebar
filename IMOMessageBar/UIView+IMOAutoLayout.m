//
//  UIView+IMOAutoLayout.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/9/4.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "UIView+IMOAutoLayout.h"

@implementation UIView (IMOAutoLayout)

- (void)prepare {
    if (!self.superview) {
        NSAssert(false, @"No superview found in %@", NSStringFromClass([self class]));
        return;
    }
    self.translatesAutoresizingMaskIntoConstraints = NO;
}

- (NSArray *)addMask:(IMOAutoLayoutMask)mask {
    return [self addMask:mask constant:0];
}

- (NSArray *)addMask:(IMOAutoLayoutMask)mask multiplier:(CGFloat)multiplier {
    return [self addMask:mask multiplier:multiplier constant:0];
}

- (NSArray *)addMask:(IMOAutoLayoutMask)mask constant:(CGFloat)constant {
    return [self addMask:mask multiplier:1 constant:constant];
}

- (NSArray *)addMask:(IMOAutoLayoutMask)mask multiplier:(CGFloat)multiplier constant:(CGFloat)constant {
    return [self addMask:mask toView:self.superview multiplier:multiplier constant:constant];
}

- (NSArray *)addMask:(IMOAutoLayoutMask)mask toView:(UIView *)view multiplier:(CGFloat)multiplier {
    return [self addMask:mask toView:view multiplier:multiplier constant:0];
}

- (NSArray *)addMask:(IMOAutoLayoutMask)mask toView:(UIView *)view constant:(CGFloat)constant {
    return [self addMask:mask toView:view multiplier:1 constant:constant];
}

- (NSArray *)addMask:(IMOAutoLayoutMask)mask toView:(UIView *)toView multiplier:(CGFloat)multiplier constant:(CGFloat)constant {
    return [self addMask:mask toView:toView toAttribute:NSLayoutAttributeNotAnAttribute multiplier:multiplier constant:constant];
}

- (NSArray *)addMask:(IMOAutoLayoutMask)mask toView:(UIView *)toView toAttribute:(NSLayoutAttribute)toAttribute multiplier:(CGFloat)multiplier constant:(CGFloat)constant {
    [self prepare];

    NSMutableArray *constraints = [[NSMutableArray alloc] init];
    
    if (mask & IMOAutoLayoutMaskEdgeAlignment) {
        mask = IMOAutoLayoutMaskTop | IMOAutoLayoutMaskBottom | IMOAutoLayoutMaskLeading | IMOAutoLayoutMaskTrailing;
    }
    if (mask & IMOAutoLayoutMaskTop) {
        NSLayoutAttribute attribute = (toAttribute == NSLayoutAttributeNotAnAttribute) ? NSLayoutAttributeTop : toAttribute;
        [constraints addObject:[self addConstraintWithView:toView fromAtrribute:NSLayoutAttributeTop toAtrribute:attribute multiplier:multiplier constant:constant]];
    }
    if (mask & IMOAutoLayoutMaskBottom) {
        NSLayoutAttribute attribute = (toAttribute == NSLayoutAttributeNotAnAttribute) ? NSLayoutAttributeBottom : toAttribute;
        [constraints addObject:[self addConstraintWithView:toView fromAtrribute:NSLayoutAttributeBottom toAtrribute:attribute multiplier:multiplier constant:constant]];
    }
    if (mask & IMOAutoLayoutMaskLeading) {
        NSLayoutAttribute attribute = (toAttribute == NSLayoutAttributeNotAnAttribute) ? NSLayoutAttributeLeading : toAttribute;
        [constraints addObject:[self addConstraintWithView:toView fromAtrribute:NSLayoutAttributeLeading toAtrribute:attribute multiplier:multiplier constant:constant]];
    }
    if (mask & IMOAutoLayoutMaskTrailing) {
        NSLayoutAttribute attribute = (toAttribute == NSLayoutAttributeNotAnAttribute) ? NSLayoutAttributeTrailing : toAttribute;
        [constraints addObject:[self addConstraintWithView:toView fromAtrribute:NSLayoutAttributeTrailing toAtrribute:attribute multiplier:multiplier constant:constant]];
    }
    if (mask & IMOAutoLayoutMaskCenterX) {
        NSLayoutAttribute attribute = (toAttribute == NSLayoutAttributeNotAnAttribute) ? NSLayoutAttributeCenterX : toAttribute;
        [constraints addObject:[self addConstraintWithView:toView fromAtrribute:NSLayoutAttributeCenterX toAtrribute:attribute multiplier:multiplier constant:constant]];
    }
    if (mask & IMOAutoLayoutMaskCenterY) {
        NSLayoutAttribute attribute = (toAttribute == NSLayoutAttributeNotAnAttribute) ? NSLayoutAttributeCenterY : toAttribute;
        [constraints addObject:[self addConstraintWithView:toView fromAtrribute:NSLayoutAttributeCenterY toAtrribute:attribute multiplier:multiplier constant:constant]];
    }
    if (mask & IMOAutoLayoutMaskWidthEqual) {
        NSLayoutAttribute toAttribute = (toView && toView != self.superview) ? NSLayoutAttributeWidth : NSLayoutAttributeNotAnAttribute;
        [constraints addObject:[self addConstraintWithView:toView fromAtrribute:NSLayoutAttributeWidth toAtrribute:toAttribute multiplier:multiplier constant:constant]];
    }
    if (mask & IMOAutoLayoutMaskHeightEqual) {
        NSLayoutAttribute toAttribute = (toView && toView != self.superview) ? NSLayoutAttributeHeight : NSLayoutAttributeNotAnAttribute;
        [constraints addObject:[self addConstraintWithView:toView fromAtrribute:NSLayoutAttributeHeight toAtrribute:toAttribute multiplier:multiplier constant:constant]];
    }
    return constraints;
}

- (NSLayoutConstraint *)addConstraintWithView:(UIView *)view
                                fromAtrribute:(NSLayoutAttribute)fromAttribute
                                  toAtrribute:(NSLayoutAttribute)toAttribute
                                   multiplier:(CGFloat)multiplier
                                     constant:(CGFloat)constant {
    id toItem = (toAttribute == NSLayoutAttributeNotAnAttribute) ? nil : view;
    NSLayoutConstraint *c = [NSLayoutConstraint constraintWithItem:self
                                                         attribute:fromAttribute
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:toItem
                                                         attribute:toAttribute
                                                        multiplier:multiplier
                                                          constant:constant];
    [self.superview addConstraint:c];
    return c;
}

- (NSArray *)constraintsForAttribute:(NSLayoutAttribute)attribute {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstAttribute = %d", attribute];
    return [self.constraints filteredArrayUsingPredicate:predicate];
}

- (NSLayoutConstraint *)constraintForAttribute:(NSLayoutAttribute)attribute firstItem:(id)first secondItem:(id)second {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstAttribute = %d AND firstItem = %@ AND secondItem = %@", attribute, first, second];
    return [[self.constraints filteredArrayUsingPredicate:predicate] firstObject];
}

@end

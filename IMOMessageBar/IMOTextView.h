//
//  IMOTextView.h
//
//  Created by spemagic on 10/1/14.
//  Copyright (c) 2014 spemagic. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kTextViewIntrinsicHeight = 30;

FOUNDATION_EXTERN NSString *const IMOTextViewContentSizeDidChangeNotification;

@interface IMOTextView : UITextView

// numberOfLines is one when text is nil
@property (nonatomic, readonly) NSUInteger numberOfLines;

@property (nonatomic, assign) NSUInteger maxNumberOfLines;

@property (nonatomic, readonly) CGFloat contentVerticalInset;

/** The placeholder text string. Default is nil. */
@property (nonatomic, copy) NSString *placeholder;

/** The placeholder color. Default is lightGrayColor. */
@property (nonatomic, copy) UIColor *placeholderColor;

@end

//
//  IMOPageItem.h
//  IMOMessage
//
//  Created by zhanghao on 14-9-24.
//  Copyright (c) 2014年 imoffice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMOPageItem : NSObject

@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *normalName;
@property (nonatomic, copy, readonly) NSString *highlightName;

+ (instancetype)itemWithNormalName:(NSString *)normalName
                     highlightName:(NSString *)highlightName
                             title:(NSString *)title;

@end

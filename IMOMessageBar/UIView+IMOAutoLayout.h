//
//  UIView+IMOAutoLayout.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/9/4.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, IMOAutoLayoutMask) {
    IMOAutoLayoutMaskEdgeAlignment = 1 << 0,        // 边缘对齐
    IMOAutoLayoutMaskTop = 1 << 1,
    IMOAutoLayoutMaskBottom = 1 << 2,
    IMOAutoLayoutMaskLeading = 1 << 3,
    IMOAutoLayoutMaskTrailing = 1 << 4,
    IMOAutoLayoutMaskCenterX = 1 << 5,
    IMOAutoLayoutMaskCenterY = 1 << 6,
    IMOAutoLayoutMaskWidthEqual = 1 << 7,
    IMOAutoLayoutMaskHeightEqual = 1 << 8,
};

#ifndef NSLayoutAttributeSameToAttribute
#define NSLayoutAttributeSameToAttribute    100
#endif

// Auto layout简易封装
@interface UIView (IMOAutoLayout)

- (NSArray *)addMask:(IMOAutoLayoutMask)mask;
- (NSArray *)addMask:(IMOAutoLayoutMask)mask multiplier:(CGFloat)multiplier;
- (NSArray *)addMask:(IMOAutoLayoutMask)mask constant:(CGFloat)constant;
- (NSArray *)addMask:(IMOAutoLayoutMask)mask multiplier:(CGFloat)multiplier constant:(CGFloat)constant;
- (NSArray *)addMask:(IMOAutoLayoutMask)mask toView:(UIView *)view multiplier:(CGFloat)multiplier;
- (NSArray *)addMask:(IMOAutoLayoutMask)mask toView:(UIView *)view constant:(CGFloat)constant;
- (NSArray *)addMask:(IMOAutoLayoutMask)mask toView:(UIView *)view multiplier:(CGFloat)multiplier constant:(CGFloat)constant;
- (NSArray *)addMask:(IMOAutoLayoutMask)mask toView:(UIView *)view toAttribute:(NSLayoutAttribute)toAttibute multiplier:(CGFloat)multiplier constant:(CGFloat)constant;


- (NSArray *)constraintsForAttribute:(NSLayoutAttribute)attribute;
- (NSLayoutConstraint *)constraintForAttribute:(NSLayoutAttribute)attribute firstItem:(id)first secondItem:(id)second;

@end

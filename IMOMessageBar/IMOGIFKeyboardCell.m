//
//  IMOGIFKeyboardCell.m
//  IMOMessageBar
//
//  Created by spemagic on 10/13/15.
//  Copyright © 2015 zhanghao. All rights reserved.
//

#import "IMOGIFKeyboardCell.h"
#import "IMOMacros.h"
#import "UIColor+IMOColor.h"
#import "IMOEmojiViewModel.h"
#import "IMOGlobalQueue.h"
#import "IMOGIFCell.h"
#import "IMOHorizontalPageLayout.h"

static NSString *const kGIFCellIdentifier = @"kGIFCellIdentifier";

@interface IMOGIFKeyboardCell () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong, readwrite) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *gifNames;

@end

@implementation IMOGIFKeyboardCell

#pragma mark - View lifecycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    NSDictionary *views = @{ @"collectionView": self.collectionView };
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|" options:0 metrics:nil views:views]];
    [self.collectionView registerClass:[IMOGIFCell class] forCellWithReuseIdentifier:kGIFCellIdentifier];
}

#pragma mark - Public methods

- (void)setEmojiModel:(IMOEmojiModel *)emojiModel {
    _emojiModel = emojiModel;
    if (emojiModel.emojiNames.count > 0) {
        self.gifNames = [NSMutableArray arrayWithArray:emojiModel.emojiNames];
        [self.collectionView reloadData];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.gifNames.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    IMOGIFCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kGIFCellIdentifier forIndexPath:indexPath];
    [cell fillCellWithImageName:self.gifNames[indexPath.row] atIndexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(gifCell:didTapGIFAtIndex:)]) {
        [self.delegate gifCell:self didTapGIFAtIndex:indexPath.row];
    }
}

#pragma mark - Getter and setter

- (NSMutableArray *)gifNames {
    if (!_gifNames) {
        _gifNames = [[NSMutableArray alloc] init];
    }
    return _gifNames;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        IMOHorizontalPageLayout *layout = [[IMOHorizontalPageLayout alloc] init];
        layout.numberOfRowsInPortrait = [IMOEmojiViewModel numberOfGIFRowsInPortrait];
        layout.numberOfColumnInPortrait = [IMOEmojiViewModel numberOfGIFColumnsInPortrait];
        layout.numberOfRowsInLandscape = [IMOEmojiViewModel numberOfGIFColumnsInPortrait];
        layout.numberOfColumnInLandscape = [IMOEmojiViewModel numberOfGIFColumnsInLandscape];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor collectionViewBackgroundColor];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.bounces = NO;
        _collectionView.scrollEnabled = NO;
        [self.contentView addSubview:_collectionView];
    }
    return _collectionView;
}

@end

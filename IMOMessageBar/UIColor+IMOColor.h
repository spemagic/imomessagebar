//
//  UIColor+IMOColor.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/9/13.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (IMOColor)

+ (UIColor *)randomColor;

+ (UIColor *)seperatorColor;

+ (UIColor *)collectionViewBackgroundColor;
+ (UIColor *)pageControlIndicatorColor;
+ (UIColor *)pageControlCurrentIndicatorColor;

@end

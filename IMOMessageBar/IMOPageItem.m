//
//  IMOPageItem.m
//  IMOMessage
//
//  Created by zhanghao on 14-9-24.
//  Copyright (c) 2014年 imoffice. All rights reserved.
//

#import "IMOPageItem.h"

@implementation IMOPageItem

+ (instancetype)itemWithNormalName:(NSString *)normalName highlightName:(NSString *)highlightName title:(NSString *)title {
    return [[self alloc] initWithNormalName:normalName highlightName:highlightName title:title];
}

- (instancetype)initWithNormalName:(NSString *)normalName highlightName:(NSString *)highlightName title:(NSString *)title {
    self = [super init];
    if (self) {
        _normalName = normalName;
        _highlightName = highlightName;
        _title = title;
    }
    return self;
}

@end

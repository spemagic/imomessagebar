//
//  IMOPageViewCell.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/9/30.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMOPageItem.h"

@protocol IMOPageViewCellDelegate;

@interface IMOPageViewCell : UICollectionViewCell

@property (nonatomic, weak) id<IMOPageViewCellDelegate> delegate;

- (void)fillCellWithItem:(IMOPageItem *)item atIndexPath:(NSIndexPath *)indexPath;

@end

@protocol IMOPageViewCellDelegate <NSObject>

@optional
- (void)didTapPageCell:(IMOPageViewCell *)pageCell;

@end

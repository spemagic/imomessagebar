//
//  IMOEmojiKeyboardToolBarCell.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/7.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMOEmojiKeyboardToolBarCell : UICollectionViewCell

@property (nonatomic, strong, readonly) UIImageView *emojiBarButtonImageView;

- (void)fillCellWithEmojiBarButtonName:(NSString *)name;

@end

//
//  IMOPageViewCell.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/9/30.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "IMOPageViewCell.h"

@implementation IMOPageViewCell

- (void)fillCellWithItem:(IMOPageItem *)item atIndexPath:(NSIndexPath *)indexPath {
    NSAssert(false, @"Subclass should override this method!");
}

@end

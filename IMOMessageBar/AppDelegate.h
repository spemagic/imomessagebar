//
//  AppDelegate.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/6.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  ViewController.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/6.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "ViewController.h"
#import "IMOMacros.h"
#import "IMOInputBar.h"
#import "IMOEmojiKeyboardToolBar.h"
#import "UIView+IMOAutoLayout.h"
#import "UIColor+IMOColor.h"
#import "IMOEmojiKeyboard.h"
#import "IMOMenuPicker.h"
#import "IMOPageView.h"
#import "IMOEmojiKeyboardCell.h"

@interface ViewController () <IMOInputBarDelegate, IMOEmojiKeyboardDataSource, IMOEmojiKeyboardDelegate>

@property (nonatomic, strong) IMOInputBar *inputBar;
@property (nonatomic, copy) NSArray *emojiNames;

@end

@implementation ViewController {
    IMOInputBar *_inputBar;
    IMOEmojiKeyboard *_emojiKeyboard;
    IMOMenuPicker *_menuPicker;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadInputBar];
//    [self loadEmojiKeyboard];
//    [self loadMenuPicker];
//    [self registerNotifications];
}

- (void)loadInputBar {
    CGFloat inputBarHeight = 50;
    _inputBar = [[IMOInputBar alloc] initWithFrame:CGRectMake(0, CGRectGetHeight([UIScreen mainScreen].bounds)-inputBarHeight, CGRectGetWidth([UIScreen mainScreen].bounds), inputBarHeight)];
    _inputBar.delegate = self;
    [self.view addSubview:_inputBar];
}

- (void)loadEmojiKeyboard {
    self.emojiNames = [IMOEmojiViewModel getAllEmojiNamesWithPortraitMode:YES];
    
    _emojiKeyboard = [[IMOEmojiKeyboard alloc] init];
    _emojiKeyboard.translatesAutoresizingMaskIntoConstraints = NO;
    _emojiKeyboard.dataSource = self;
    _emojiKeyboard.delegate = self;
    [self.view addSubview:_emojiKeyboard];
    
    NSDictionary *views = @{ @"emojiKeyboard": _emojiKeyboard };
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[emojiKeyboard]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[emojiKeyboard(216)]|" options:0 metrics:nil views:views]];
}

- (void)loadMenuPicker {
    if (!_menuPicker) {
        _menuPicker = [[IMOMenuPicker alloc] initWithFrame:CGRectZero];
        _menuPicker.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:_menuPicker];
        
        NSDictionary *views = @{ @"menuPicker": _menuPicker };
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[menuPicker]|" options:0 metrics:nil views:views]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-64-[menuPicker(216)]" options:0 metrics:nil views:views]];
        
        NSMutableArray *items = [NSMutableArray array];
        NSArray *titles = @[@"照片", @"拍照", @"@提醒", @"位置", @"发任务", @"轻审批"];
        NSArray *normalImages = @[@"chat_ex_pic_normal", @"chat_ex_camera_normal", @"chat_ex_at_normal", @"chat_ex_location_normal", @"chat_ex_task_normal", @"chat_ex_approval_normal"];
        NSArray *highlightImages = @[@"chat_ex_pic_down", @"chat_ex_camera_down", @"chat_ex_at_down", @"chat_ex_location_down", @"chat_ex_task_down", @"chat_ex_approval_down"];
        for (int i = 0; i < 5; i++) {
            for (int i = 0; i < titles.count; i++) {
                IMOPageItem *item = [IMOPageItem itemWithNormalName:normalImages[i]
                                                      highlightName:highlightImages[i]
                                                              title:titles[i]];
                [items addObject:item];
            }
        }
        _menuPicker.pageItems = items;
    }
}

- (NSArray *)emojiPickerImageNamesInEmojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard {
    return self.emojiNames;
}

- (NSArray *)bottomToolBarImageNamesInEmojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard {
    return @[@"emoji_tool_bar_button_emoji", @"ban_che_xiao_zi"];
}

- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard didTapEmojiAtIndexPath:(NSIndexPath *)indexPath subIndex:(NSInteger)subIndex {
    IMOEmojiModel *emojiModel = self.emojiNames[indexPath.section][indexPath.row];
    NSString *emojiName = emojiModel.emojiNames[subIndex];
    
    BOOL enable = ![emojiName isEqualToString:kEmojiPickerDeleteButtonKey];
    [emojiKeyboard enableSendButton:enable];
}

- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard didTapSendButton:(UIButton *)sendButton {

}

- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard didTapEmojiBarButtonAtIndex:(NSInteger)index isSameIndex:(BOOL)isSameIndex {
    
}

- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard willShowEmojiPageAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidRotateNotification:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

- (void)unregisterNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

- (void)applicationDidRotateNotification:(NSNotification *)notification {
    UIInterfaceOrientation orientation = (UIInterfaceOrientation)[notification.userInfo[UIApplicationStatusBarOrientationUserInfoKey] integerValue];
    if (orientation == UIInterfaceOrientationUnknown) {
        return;
    }
    
    self.emojiNames = [IMOEmojiViewModel getAllEmojiNamesWithPortraitMode:IMOInterfaceOrientationIsPortrait];
    [_emojiKeyboard reloadData];
}

- (IBAction)buttonAction:(id)sender {
    UIViewController *vc = [[UIViewController alloc] init];
    vc.view.backgroundColor = [UIColor cyanColor];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [_inputBar switchToInteractionMode:IMOInputBarInteractionModeStopInput animated:YES];
}

@end

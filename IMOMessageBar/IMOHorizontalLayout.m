//
//  IMOHorizontalLayout.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/7.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "IMOHorizontalLayout.h"

@interface IMOHorizontalLayout ()

@property (nonatomic, copy) NSArray *layoutAttributes;
@property (nonatomic, assign) CGSize itemSize;

@end

@implementation IMOHorizontalLayout

#pragma mark - Object lifecycle

- (instancetype)initWithItemWidth:(CGFloat)itemWidth {
    self = [super init];
    if (self) {
        _itemSize = CGSizeMake(itemWidth, 0);
    }
    return self;
}

- (CGSize)itemSize {
    if (self.updateItemSize) {
        _itemSize.width = CGRectGetWidth(self.collectionView.bounds);
    }
    return CGSizeMake(_itemSize.width, CGRectGetHeight(self.collectionView.bounds));
}

#pragma mark - Override layout methods

- (void)prepareLayout {
    CGSize size = self.itemSize;
    size.height = CGRectGetHeight(self.collectionView.bounds);
    self.itemSize = size;
    
    NSMutableArray *layoutAttributesArray = [NSMutableArray array];
    
    NSInteger cellIndex = -1;
    NSInteger sections = [self.collectionView numberOfSections];
    for (NSInteger section = 0; section < sections; section++) {
        NSInteger items = [self.collectionView numberOfItemsInSection:section];
        
        for (NSInteger item = 0; item < items; item++) {
            cellIndex++;
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            UICollectionViewLayoutAttributes *attrbute = [self layoutAttributesForItemAtIndexPath:indexPath atCellIndex:cellIndex];
            [layoutAttributesArray addObject:attrbute];
        }
    }

    self.layoutAttributes = [NSArray arrayWithArray:layoutAttributesArray];
}

- (CGSize)collectionViewContentSize {
    CGSize size = CGSizeMake(self.itemSize.width * self.layoutAttributes.count, CGRectGetHeight(self.collectionView.bounds));
    return size;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    return self.layoutAttributes;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return NO;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath atCellIndex:(NSInteger)cellIndex {
    UICollectionViewLayoutAttributes *attrbute = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    CGRect frame = CGRectMake(cellIndex * self.itemSize.width, 0, self.itemSize.width, self.itemSize.height);
    attrbute.frame = frame;
    attrbute.zIndex = indexPath.item;
    return attrbute;
}

@end

//
//  IMOEmojiKeyboard.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/7.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMOEmojiViewModel.h"
#import "IMOEmojiModel.h"

@protocol IMOEmojiKeyboardDataSource;
@protocol IMOEmojiKeyboardDelegate;

@class IMOEmojiKeyboardToolBar;

@interface IMOEmojiKeyboard : UIView

@property (nonatomic, weak) IBOutlet id<IMOEmojiKeyboardDataSource> dataSource;
@property (nonatomic, weak) IBOutlet id<IMOEmojiKeyboardDelegate> delegate;

@property (nonatomic, readonly) UICollectionView *collectionView;
@property (nonatomic, readonly) UIPageControl *pageControl;

@property (nonatomic, readonly) IMOEmojiKeyboardToolBar *emojiToolBar;

- (void)reloadData;

- (void)enableSendButton:(BOOL)enable;
- (void)showSendButton:(BOOL)show animated:(BOOL)animated;

@end

@protocol IMOEmojiKeyboardDataSource <NSObject>

@required
// NSString组成的一维数组
- (NSArray *)bottomToolBarImageNamesInEmojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard;

// IMOEmojiModel组成的二维数组
- (NSArray *)emojiPickerImageNamesInEmojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard;

@end

@protocol IMOEmojiKeyboardDelegate <NSObject>

@optional
// emoji tool bar
- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard didTapSendButton:(UIButton *)sendButton;
- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard didTapEmojiBarButtonAtIndex:(NSInteger)index isSameIndex:(BOOL)isSameIndex;
- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard willShowEmojiPageAtIndexPath:(NSIndexPath *)indexPath;

- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard didTapEmojiAtIndexPath:(NSIndexPath *)indexPath subIndex:(NSInteger)subIndex;

@end
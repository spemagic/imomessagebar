//
//  IMOMenuPicker.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/9/28.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "IMOMenuPicker.h"
#import "IMOMacros.h"
#import "IMOPageView.h"
#import "IMOMenuCell.h"
#import "IMOHorizontalPageLayout.h"
#import "UIView+IMOAutoLayout.h"
#import "UIColor+IMOColor.h"

@interface IMOMenuPicker () <IMOPageViewDataSource, IMOPageViewDelegate>

@property (nonatomic, strong, readwrite) IMOPageView *pageView;
@property (nonatomic, strong, readwrite) UIPageControl *pageControl;
@property (nonatomic, strong) UIView *topSeperatorView;

@end

@implementation IMOMenuPicker

#pragma mark - View lifecycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    self.backgroundColor = [UIColor collectionViewBackgroundColor];
    
    NSDictionary *views = @{ @"topSeperatorView": self.topSeperatorView,
                             @"pageView": self.pageView,
                             @"pageControl": self.pageControl
                             };
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[topSeperatorView]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pageControl]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pageView]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[topSeperatorView(0.2)][pageView][pageControl(30)]|" options:0 metrics:nil views:views]];
}

- (NSUInteger)pageOfMenus {
    if (_pageItems.count > 0) {
        NSInteger menusOfPerPage = self.pageView.pageViewLayout.numberOfRows * self.pageView.pageViewLayout.numberOfColumn;
        return (NSUInteger)ceil(_pageItems.count / (CGFloat)menusOfPerPage);
    }
    return 0;
}

#pragma mark - Public methods

- (void)setPageItems:(NSArray *)pageItems {
    if (_pageItems != pageItems) {
        _pageItems = [pageItems copy];
        [self reloadData];
    }
}

- (void)reloadData {
    if (_pageItems.count > 0) {
        [self.pageView reloadData];
        self.pageControl.numberOfPages = [self pageOfMenus];
    }
}

- (NSString *)titleAtIndex:(NSInteger)index {
    if (index < self.pageItems.count) {
        IMOPageItem *item = self.pageItems[index];
        return item.title;
    }
    return nil;
}

#pragma mark - IMOPageViewDataSource

- (Class)pageViewCellClass:(IMOPageView *)pageView {
    return [IMOMenuCell class];
}

- (IMOHorizontalPageLayout *)pageViewLayout:(IMOPageView *)pageView {
    IMOHorizontalPageLayout *pageLayout = [[IMOHorizontalPageLayout alloc] init];
    pageLayout.numberOfRowsInPortrait = 2;
    pageLayout.numberOfColumnInPortrait = 4;
    pageLayout.numberOfRowsInLandscape = 2;
    pageLayout.numberOfColumnInLandscape = 4;
    return pageLayout;
}

- (NSArray *)pageViewItems:(IMOPageView *)pageView {
    return self.pageItems;
}

#pragma mark - IMOPageViewDelegate

- (void)pageView:(IMOPageView *)pageView didTapCellAtIndex:(NSInteger)index {
    if ([self.delegate respondsToSelector:@selector(menuPicker:didTapMenuIndex:)]) {
        [self.delegate menuPicker:self didTapMenuIndex:index];
    }
}

- (void)pageView:(IMOPageView *)pageView scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger currentPage = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    [self.pageControl setCurrentPage:currentPage];
}

#pragma mark - Getter and setter

- (IMOPageView *)pageView {
    if (!_pageView) {
        _pageView = [[IMOPageView alloc] init];
        _pageView.translatesAutoresizingMaskIntoConstraints = NO;
        _pageView.dataSource = self;
        _pageView.delegate = self;
        [self addSubview:_pageView];
    }
    return _pageView;
}

- (UIPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.hidesForSinglePage = YES;
        _pageControl.translatesAutoresizingMaskIntoConstraints = NO;
        _pageControl.pageIndicatorTintColor = [UIColor pageControlIndicatorColor];
        _pageControl.currentPageIndicatorTintColor = [UIColor pageControlCurrentIndicatorColor];
        [self addSubview:_pageControl];
    }
    return _pageControl;
}

- (UIView *)topSeperatorView {
    if (!_topSeperatorView) {
        _topSeperatorView = [UIView new];
        _topSeperatorView.translatesAutoresizingMaskIntoConstraints = NO;
        _topSeperatorView.backgroundColor = RGB(220, 220, 220);
        [self addSubview:_topSeperatorView];
    }
    return _topSeperatorView;
}

@end

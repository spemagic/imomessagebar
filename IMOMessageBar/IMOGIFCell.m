//
//  IMOGIFCell.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/9/28.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "IMOGIFCell.h"
#import "IMOMacros.h"
#import "UIView+IMOAutoLayout.h"
#import "IMOGlobalQueue.h"
#import "FLAnimatedImage.h"

@interface IMOGIFCell ()

@property (nonatomic, strong, readwrite) UIImageView *emojiImageView;

@end

@implementation IMOGIFCell

#pragma mark - View lifecycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat imageWidth = 80;
        [self.emojiImageView addMask:IMOAutoLayoutMaskCenterX];
        [self.emojiImageView addMask:IMOAutoLayoutMaskCenterY];
        [self.emojiImageView addMask:IMOAutoLayoutMaskWidthEqual constant:imageWidth];
        [self.emojiImageView addMask:IMOAutoLayoutMaskHeightEqual constant:imageWidth];
    }
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.emojiImageView.image = nil;
}

#pragma mark - Public methods

- (void)fillCellWithImageName:(NSString *)name atIndexPath:(NSIndexPath *)indexPath {
    dispatch_async_resource_queue((^{
        UIImage *image = [UIImage imageNamed:name];
        dispatch_main_async_safe(^{
            self.emojiImageView.image = image;
        });
    }));
}

#pragma mark - Getter and setter

- (UIImageView *)emojiImageView {
    if (!_emojiImageView) {
        _emojiImageView = [UIImageView new];
        _emojiImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_emojiImageView];
    }
    return _emojiImageView;
}

@end

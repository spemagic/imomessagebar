//
//  IMOEmojiKeyboardCell.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/7.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMOEmojiModel.h"

@protocol IMOEmojiKeyboardCellDelegate;

@interface IMOEmojiKeyboardCell : UICollectionViewCell

@property (nonatomic, weak) id<IMOEmojiKeyboardCellDelegate> delegate;
@property (nonatomic, strong) IMOEmojiModel *emojiModel;

@property (nonatomic, readonly) UICollectionView *collectionView;

@end


@protocol IMOEmojiKeyboardCellDelegate <NSObject>

@optional
- (void)emojiCell:(IMOEmojiKeyboardCell *)cell didTapEmojiAtIndex:(NSInteger)index;

@end
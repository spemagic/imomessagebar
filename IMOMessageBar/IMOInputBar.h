//
//  IMOInputBar.h
//  IMOMessageBar
//
//  Created spemagic 7/7/15.
//  Copyright (c) 2015 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, IMOInputBarInteractionMode) {
    IMOInputBarInteractionModeUnknown,      // 未知模式
    IMOInputBarInteractionModeNormal,       // 正常模式
    IMOInputBarInteractionModeVoice,        // 语音模式
    IMOInputBarInteractionModeInput,        // 文字输入模式
    IMOInputBarInteractionModeEmoji,        // emoji表情模式
    IMOInputBarInteractionModeMenu,         // 菜单栏模式
    IMOInputBarInteractionModeStopInput,    // 停止输入模式，该模式与正常模式的区别是该模式不会强制隐藏语音输入按钮
};

typedef NS_ENUM(NSUInteger, IMOInputBarButton) {
    IMOInputBarButtonUnknown,               // 未知
    IMOInputBarButtonVoice,                 // 语音按钮
    IMOInputBarButtonEmoji,                 // emoji表情按钮
    IMOInputBarButtonMenu,                  // 菜单按钮
};

FOUNDATION_EXTERN const CGFloat kMessageInputBarNormalHeight;

@protocol IMOInputBarDelegate;

@interface IMOInputBar : UIView

@property (nonatomic, readonly) UITextView *textView;
@property (nonatomic, weak) IBOutlet id<IMOInputBarDelegate> delegate;

/**
 切换交互模式，不同交互模式不同的界面表现
 
 @param mode 自定义交互模式
 */
- (void)switchToInteractionMode:(IMOInputBarInteractionMode)mode animated:(BOOL)animated;

@end


@protocol IMOInputBarDelegate <NSObject>

@optional
- (void)inputBar:(IMOInputBar *)inputBar didTapButton:(IMOInputBarButton)buttonType;

@end
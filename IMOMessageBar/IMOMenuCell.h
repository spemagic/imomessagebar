//
//  IMOMenuCell.h
//  imoffice
//
//  Created by zhanghao on 15/7/6.
//  Copyright (c) 2015年 IMO. All rights reserved.
//

#import "IMOPageViewCell.h"

@interface IMOMenuCell : IMOPageViewCell

@property (nonatomic, readonly) UIButton *menuButton;
@property (nonatomic, readonly) UILabel *menuTitleLabel;

@end

//
//  IMOPageView.h
//
//
//  Created by zhanghao on 15/7/6.
//  Copyright (c) 2015年 IMO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMOHorizontalPageLayout.h"
#import "IMOPageItem.h"

@protocol IMOPageViewDataSource;
@protocol IMOPageViewDelegate;

@interface IMOPageView : UIView

@property (nonatomic, weak) id<IMOPageViewDataSource> dataSource;
@property (nonatomic, weak) id<IMOPageViewDelegate> delegate;

@property (nonatomic, readonly) UICollectionView *collectionView;
@property (nonatomic, readonly) IMOHorizontalPageLayout *pageViewLayout;

- (void)reloadData;
- (void)resetContentOffsetAnimated:(BOOL)animated;

@end

@protocol IMOPageViewDataSource <NSObject>

@required

// 该函数只调用一次
- (Class)pageViewCellClass:(IMOPageView *)pageView;

// 该函数只调用一次
- (IMOHorizontalPageLayout *)pageViewLayout:(IMOPageView *)pageView;

// 该函数会调用多次
- (NSArray *)pageViewItems:(IMOPageView *)pageView;

@end

@protocol IMOPageViewDelegate <NSObject>

@optional
- (void)pageView:(IMOPageView *)pageView didSelectItemAtIndex:(NSInteger)index;
- (void)pageView:(IMOPageView *)pageView didTapCellAtIndex:(NSInteger)index;

- (void)pageView:(IMOPageView *)pageView scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)pageView:(IMOPageView *)pageView scrollViewDidEndDecelerating:(UIScrollView *)scrollView;

@end

//
//  IMOEmojiCell.m
//  IMOMessageBar
//
//  Created by spemagic on 10/13/15.
//  Copyright © 2015 zhanghao. All rights reserved.
//

#import "IMOEmojiCell.h"
#import "IMOMacros.h"
#import "UIView+IMOAutoLayout.h"
#import "IMOGlobalQueue.h"
#import "IMOImageCache.h"

@interface IMOEmojiCell ()

@property (nonatomic, strong, readwrite) UIImageView *emojiImageView;
@property (nonatomic, strong) IMOImageCache *imageCache;

@end

@implementation IMOEmojiCell

#pragma mark - View lifecycle

- (void)dealloc {
    [_imageCache removeAllCaches];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat imageWidth = 30;
        [self.emojiImageView addMask:IMOAutoLayoutMaskCenterX];
        [self.emojiImageView addMask:IMOAutoLayoutMaskCenterY];
        [self.emojiImageView addMask:IMOAutoLayoutMaskWidthEqual constant:imageWidth];
        [self.emojiImageView addMask:IMOAutoLayoutMaskHeightEqual constant:imageWidth];
    }
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.emojiImageView.image = nil;
}

#pragma mark - Public methods

- (void)fillCellWithImageName:(NSString *)name atIndexPath:(NSIndexPath *)indexPath {
    if (!name) {
        return;
    }
    
    UIImage *cacheImage = [self.imageCache queryImageWithKey:name];
    if (cacheImage) {
        self.emojiImageView.image = cacheImage;
    } else {
        dispatch_async_resource_queue((^{
            UIImage *image = [UIImage imageNamed:name];
            if (image) {
                [self.imageCache storeImage:image forKey:name];
            }
            dispatch_main_async_safe(^{
                self.emojiImageView.image = image;
            });
        }));
    }
}

#pragma mark - Getter and setter

- (UIImageView *)emojiImageView {
    if (!_emojiImageView) {
        _emojiImageView = [UIImageView new];
        _emojiImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_emojiImageView];
    }
    return _emojiImageView;
}

- (IMOImageCache *)imageCache {
    if (!_imageCache) {
        _imageCache = [[IMOImageCache alloc] init];
    }
    return _imageCache;
}

@end

//
//  IMOMenuPicker.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/9/28.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMOPageItem.h"

@class IMOPageView;

@protocol IMOMenuPickerDelegate;

@interface IMOMenuPicker : UIView

@property (nonatomic, weak) id<IMOMenuPickerDelegate> delegate;

@property (nonatomic, readonly) IMOPageView *pageView;
@property (nonatomic, readonly) UIPageControl *pageControl;

/**
 *  数组元素为 IMOPageItem，set之后会调用reloadData函数
 */
@property (nonatomic, copy) NSArray *pageItems;

/**
 *  刷新数据
 */
- (void)reloadData;

/**
 *  获取index对应button的标题，有可能为nil
 *
 *  @param index 按钮对应的index
 *
 *  @return 按钮对应的标题
 */
- (NSString *)titleAtIndex:(NSInteger)index;

@end



@protocol IMOMenuPickerDelegate <NSObject>

@optional
- (void)menuPicker:(IMOMenuPicker *)menuPicker didTapMenuIndex:(NSUInteger)index;

@end

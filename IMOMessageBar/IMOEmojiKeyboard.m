//
//  IMOEmojiKeyboard.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/7.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "IMOEmojiKeyboard.h"
#import "IMOEmojiKeyboardToolBar.h"
#import "IMOHorizontalLayout.h"
#import "IMOHorizontalPageLayout.h"
#import "IMOEmojiKeyboardCell.h"
#import "IMOGIFKeyboardCell.h"
#import "UIColor+IMOColor.h"
#import "IMOEmojiModel.h"
#import "IMOMacros.h"

static NSString *const kEmojiKeyboardCellIdentifier = @"kEmojiKeyboardCellIdentifier";
static NSString *const kGIFKeyboardCellIdentifier = @"kGIFKeyboardCellIdentifier";

@interface IMOEmojiKeyboard () <IMOEmojiKeyboardToolBarDelegate, IMOEmojiKeyboardCellDelegate, IMOGIFKeyboardCellDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong, readwrite) UICollectionView *collectionView;
@property (nonatomic, strong, readwrite) UIPageControl *pageControl;
@property (nonatomic, strong, readwrite) IMOEmojiKeyboardToolBar *emojiToolBar;

@property (nonatomic, strong) UIView *topSeperatorView;
@property (nonatomic, copy) NSArray *emojiModels;

@end

@implementation IMOEmojiKeyboard {
    NSIndexPath *_currentIndexPath;
}

#pragma mark - View lifecycle

- (void)dealloc {
    [self unregisterNotifications];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    self.backgroundColor = [UIColor collectionViewBackgroundColor];
    
    NSDictionary *metrics = @{ @"toolBarHeight" : @36,
                               @"pageHeight" : @12,
                               @"pagePadding" : @10
                               };
    NSDictionary *views = @{ @"topSeperatorView": self.topSeperatorView,
                             @"collectionView" : self.collectionView,
                             @"pageControl" : self.pageControl,
                             @"emojiToolBar" : self.emojiToolBar
                             };
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[topSeperatorView]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pageControl]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[emojiToolBar]|" options:0 metrics:nil views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[topSeperatorView(0.2)][collectionView][pageControl(pageHeight)]-(pagePadding)-[emojiToolBar(toolBarHeight)]|" options:0 metrics:metrics views:views]];

    [self.collectionView registerClass:[IMOEmojiKeyboardCell class] forCellWithReuseIdentifier:kEmojiKeyboardCellIdentifier];
    [self.collectionView registerClass:[IMOGIFKeyboardCell class] forCellWithReuseIdentifier:kGIFKeyboardCellIdentifier];
    
//#define EMOJI_KEYBOARD_SUPPORT_LANDSCAPE_MODE
    
#ifdef EMOJI_KEYBOARD_SUPPORT_LANDSCAPE_MODE
    [self registerNotifications];
#endif
}

- (NSIndexPath *)indexPathWithContentOffsetX:(CGFloat)x {
    return [self.collectionView indexPathForItemAtPoint:CGPointMake(x, 10)];
}

- (NSInteger)numberOfPagesWithSection:(NSInteger)section {
    if (section < self.emojiModels.count) {
        NSArray *sections = self.emojiModels[section];
        return sections.count;
    }
    return 0;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.emojiModels.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSArray *items = self.emojiModels[section];
    return items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    IMOEmojiModel *model = self.emojiModels[indexPath.section][indexPath.row];
    if (model.isGIF) {
        IMOGIFKeyboardCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kGIFKeyboardCellIdentifier forIndexPath:indexPath];
        cell.delegate = self;
        cell.emojiModel = model;
        return cell;
    } else {
        IMOEmojiKeyboardCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kEmojiKeyboardCellIdentifier forIndexPath:indexPath];
        cell.delegate = self;
        cell.emojiModel = model;
        return cell;
    }
    return nil;
}

#pragma mark - UICollectionViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSIndexPath *indexPath = [self indexPathWithContentOffsetX:scrollView.contentOffset.x];
    if (indexPath) {
        if (!_currentIndexPath || ![_currentIndexPath isEqual:indexPath]) {
            if ([self.delegate respondsToSelector:@selector(emojiKeyboard:willShowEmojiPageAtIndexPath:)]) {
                _currentIndexPath = indexPath;
                [self.delegate emojiKeyboard:self willShowEmojiPageAtIndexPath:indexPath];
                
                NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:indexPath.section inSection:0];
                [self.emojiToolBar.collectionView selectItemAtIndexPath:selectedIndexPath animated:NO scrollPosition:UICollectionViewScrollPositionLeft];
            }
            self.pageControl.numberOfPages = [self numberOfPagesWithSection:indexPath.section];
            self.pageControl.currentPage = indexPath.row;
        }
    }
}

#pragma mark - IMOGIFCellDelegate

- (void)gifCell:(IMOGIFKeyboardCell *)cell didTapGIFAtIndex:(NSInteger)index {
    if ([self.delegate respondsToSelector:@selector(emojiKeyboard:didTapEmojiAtIndexPath:subIndex:)]) {
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        [self.delegate emojiKeyboard:self didTapEmojiAtIndexPath:indexPath subIndex:index];
    }
}

- (void)emojiCell:(IMOEmojiKeyboardCell *)cell didTapEmojiAtIndex:(NSInteger)index {
    if ([self.delegate respondsToSelector:@selector(emojiKeyboard:didTapEmojiAtIndexPath:subIndex:)]) {
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        [self.delegate emojiKeyboard:self didTapEmojiAtIndexPath:indexPath subIndex:index];
    }
}

#pragma mark - IMOEmojiToolBarDelegate

- (void)emojiToolBar:(IMOEmojiKeyboardToolBar *)emojiToolBar didTapBarButtonIndex:(NSUInteger)index isSameIndex:(BOOL)isSameIndex {
    if (!isSameIndex) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:index];
        [self.collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionLeft];
    }
    
    if ([self.delegate respondsToSelector:@selector(emojiKeyboard:didTapEmojiBarButtonAtIndex:isSameIndex:)]) {
        [self.delegate emojiKeyboard:self didTapEmojiBarButtonAtIndex:index isSameIndex:isSameIndex];
    }
}

- (void)emojiToolBar:(IMOEmojiKeyboardToolBar *)emojiToolBar didTapSendButton:(UIButton *)sendButton {
    if ([self.delegate respondsToSelector:@selector(emojiKeyboard:didTapSendButton:)]) {
        [self.delegate emojiKeyboard:self didTapSendButton:sendButton];
    }
}

#pragma mark - Public methods

- (void)setDataSource:(id<IMOEmojiKeyboardDataSource>)dataSource {
    if (_dataSource != dataSource) {
        _dataSource = dataSource;
        [self reloadData];
    }
}

- (void)reloadData {
    if ([self.dataSource respondsToSelector:@selector(bottomToolBarImageNamesInEmojiKeyboard:)]) {
        NSArray *toolBarImages = [self.dataSource bottomToolBarImageNamesInEmojiKeyboard:self];
        if (toolBarImages.count > 0) {
            self.emojiToolBar.emojiBarButtonNames = toolBarImages;
        }
    }
    
    if ([self.dataSource respondsToSelector:@selector(emojiPickerImageNamesInEmojiKeyboard:)]) {
        NSArray *emojiImages = [self.dataSource emojiPickerImageNamesInEmojiKeyboard:self];
        if (emojiImages.count > 0) {
            self.emojiModels = emojiImages;
            self.pageControl.currentPage = 0;
            self.pageControl.numberOfPages = [self numberOfPagesWithSection:0];
            [self.collectionView reloadData];
        }
    }
}

- (void)showSendButton:(BOOL)show animated:(BOOL)animated {
    [self.emojiToolBar showSendButton:show animated:animated];
}

- (void)enableSendButton:(BOOL)enable {
    self.emojiToolBar.enableSend = enable;
}

#pragma mark - Notifications

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidRotateNotification:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

- (void)unregisterNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

- (void)applicationDidRotateNotification:(NSNotification *)notification {
    if (!self.superview || self.isHidden) {
        return;
    }
    
    UIInterfaceOrientation orientation = (UIInterfaceOrientation)[notification.userInfo[UIApplicationStatusBarOrientationUserInfoKey] integerValue];
    if (orientation == UIInterfaceOrientationUnknown) {
        return;
    }
}

#pragma mark - Getter and setter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        IMOHorizontalLayout *layout = [[IMOHorizontalLayout alloc] initWithItemWidth:CGRectGetWidth([UIScreen mainScreen].bounds)];
        layout.updateItemSize = YES;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor collectionViewBackgroundColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.scrollsToTop = NO;
        _collectionView.pagingEnabled = YES;
        _collectionView.bounces = YES;
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

- (UIPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.hidesForSinglePage = YES;
        _pageControl.pageIndicatorTintColor = [UIColor pageControlIndicatorColor];
        _pageControl.currentPageIndicatorTintColor = [UIColor pageControlCurrentIndicatorColor];
        _pageControl.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_pageControl];
    }
    return _pageControl;
}

- (IMOEmojiKeyboardToolBar *)emojiToolBar {
    if (!_emojiToolBar) {
        _emojiToolBar = [[IMOEmojiKeyboardToolBar alloc] init];
        _emojiToolBar.delegate = self;
        _emojiToolBar.translatesAutoresizingMaskIntoConstraints = NO;
        _emojiToolBar.backgroundColor = [UIColor collectionViewBackgroundColor];
        [self addSubview:_emojiToolBar];
    }
    return _emojiToolBar;
}

- (UIView *)topSeperatorView {
    if (!_topSeperatorView) {
        _topSeperatorView = [UIView new];
        _topSeperatorView.translatesAutoresizingMaskIntoConstraints = NO;
        _topSeperatorView.backgroundColor = RGB(220, 220, 220);
        [self addSubview:_topSeperatorView];
    }
    return _topSeperatorView;
}

@end

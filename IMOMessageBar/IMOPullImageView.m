//
//  IMOPullImageView.m
//  imoffice
//
//  Created by DingXiao on 15/6/3.
//  Copyright (c) 2015年 IMO. All rights reserved.
//

#import "IMOPullImageView.h"
#import <CoreText/CoreText.h>

@implementation IMOPullImageView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        /* 纯粹没事找事，谁见了不爽，换个lable 😄*/
        UIFont *font = [UIFont systemFontOfSize:17];
        NSString *text = @"发任务模式";
        CGRect rect = [text boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        
        CATextLayer *textLayer = [CATextLayer layer];
        textLayer.frame = rect;
        textLayer.position = CGPointMake(frame.size.width/2,frame.size.height/2);
        textLayer.contentsScale = [UIScreen mainScreen].scale;
        [self.layer addSublayer:textLayer];
        
        //set text attributes
        textLayer.alignmentMode = kCAAlignmentCenter;
        textLayer.wrapped = YES;
        
        //create attributed string
        NSMutableAttributedString *string = nil;
        string = [[NSMutableAttributedString alloc] initWithString:text];
        
        //convert UIFont to a CTFont
        CFStringRef fontName = (__bridge CFStringRef)font.fontName;
        CGFloat fontSize = font.pointSize;
        CTFontRef fontRef = CTFontCreateWithName(fontName, fontSize, NULL);
        
        //set text attributes
        NSDictionary *attribs = @{
                                  (__bridge id)kCTForegroundColorAttributeName:(__bridge id)[UIColor whiteColor].CGColor,
                                  (__bridge id)kCTFontAttributeName: (__bridge id)fontRef
                                  };
        
        [string setAttributes:attribs range:NSMakeRange(0, [text length])];
        //release the CTFont we created earlier
        CFRelease(fontRef);
        
        //set layer text
        textLayer.string = string;
    }
    return self;
}
@end

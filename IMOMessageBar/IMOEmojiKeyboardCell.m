//
//  IMOEmojiKeyboardCell.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/7.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "IMOEmojiKeyboardCell.h"
#import "IMOMacros.h"
#import "UIColor+IMOColor.h"
#import "IMOEmojiViewModel.h"
#import "IMOGlobalQueue.h"
#import "IMOEmojiCell.h"
#import "IMOHorizontalPageLayout.h"

static NSString *const kEmojiCellIdentifier = @"kEmojiCellIdentifier";

@interface IMOEmojiKeyboardCell () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong, readwrite) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *emojiNames;

@end

@implementation IMOEmojiKeyboardCell

#pragma mark - View lifecycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    NSDictionary *views = @{ @"collectionView": self.collectionView };
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|" options:0 metrics:nil views:views]];
    [self.collectionView registerClass:[IMOEmojiCell class] forCellWithReuseIdentifier:kEmojiCellIdentifier];
}

#pragma mark - Public methods

- (void)setEmojiModel:(IMOEmojiModel *)emojiModel {
    _emojiModel = emojiModel;
    if (emojiModel.emojiNames.count > 0) {
        self.emojiNames = [NSMutableArray arrayWithArray:emojiModel.emojiNames];
        [self.collectionView reloadData];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.emojiNames.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    IMOEmojiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kEmojiCellIdentifier forIndexPath:indexPath];
    [cell fillCellWithImageName:self.emojiNames[indexPath.row] atIndexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(emojiCell:didTapEmojiAtIndex:)]) {
        [self.delegate emojiCell:self didTapEmojiAtIndex:indexPath.row];
    }
}

#pragma mark - Getter and setter

- (NSMutableArray *)emojiNames {
    if (!_emojiNames) {
        _emojiNames = [[NSMutableArray alloc] init];
    }
    return _emojiNames;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        IMOHorizontalPageLayout *layout = [[IMOHorizontalPageLayout alloc] init];
        layout.numberOfRowsInPortrait = [IMOEmojiViewModel numberOfEmojiRowsInPortrait];
        layout.numberOfColumnInPortrait = [IMOEmojiViewModel numberOfEmojiColumnsInPortrait];
        layout.numberOfRowsInLandscape = [IMOEmojiViewModel numberOfEmojiRowsInLandscape];
        layout.numberOfColumnInLandscape = [IMOEmojiViewModel numberOfEmojiColumnsInLandscape];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor collectionViewBackgroundColor];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.bounces = NO;
        _collectionView.scrollEnabled = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [self.contentView addSubview:_collectionView];
    }
    return _collectionView;
}

@end

//
//  UIImage+IMOImage.m
//  IMOWorkchatFramework
//
//  Created by zhanghao on 15/8/28.
//  Copyright (c) 2015年 imoffice. All rights reserved.
//

#import "UIImage+IMOImage.h"

@implementation UIImage (IMOImage)

+ (UIImage *)imo_imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end

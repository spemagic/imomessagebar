//
//  UIImage+IMOImage.h
//  IMOWorkchatFramework
//
//  Created by zhanghao on 15/8/28.
//  Copyright (c) 2015年 imoffice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (IMOImage)

+ (UIImage *)imo_imageWithColor:(UIColor *)color;

@end

//
//  IMOPageView.m
//  imoffice
//
//  Created by zhanghao on 15/7/6.
//  Copyright (c) 2015年 IMO. All rights reserved.
//

#import "IMOPageView.h"
#import "IMOMacros.h"
#import "UIColor+IMOColor.h"
#import "IMOPageViewCell.h"

@interface IMOPageView () <UICollectionViewDataSource, UICollectionViewDelegate, IMOPageViewCellDelegate>

@property (nonatomic, strong, readwrite) UICollectionView *collectionView;
@property (nonatomic, strong, readwrite) IMOHorizontalPageLayout *pageViewLayout;

@property (nonatomic, copy) NSArray *pageItems;

@end

@implementation IMOPageView {
    Class _cellClass;
}

#pragma mark - View lifecycle

- (void)dealloc {
    _collectionView.dataSource = nil;
    _collectionView.delegate = nil;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    NSDictionary *views = @{ @"collectionView": self.collectionView };
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|" options:0 metrics:nil views:views]];
}

#pragma mark - Public methods

- (void)setDataSource:(id<IMOPageViewDataSource>)dataSource {
    if (_dataSource != dataSource) {
        _dataSource = dataSource;
        [self reloadData];
    }
}

- (void)reloadData {
    if (!_cellClass) {
        if ([self.dataSource respondsToSelector:@selector(pageViewCellClass:)]) {
            _cellClass = [self.dataSource pageViewCellClass:self];
        }
        if (!_cellClass) {
            IMOThrowExcptionWithReason(@"pageViewCellClass must return a non-nil cell class!");
            return;
        }
        [self registerCell:_cellClass];
    }
    
    if (!self.pageViewLayout) {
        if ([self.dataSource respondsToSelector:@selector(pageViewLayout:)]) {
            self.pageViewLayout = [self.dataSource pageViewLayout:self];
            self.collectionView.collectionViewLayout = self.pageViewLayout;
        }
        self.collectionView.dataSource = self;
    }
    
    if ([self.dataSource respondsToSelector:@selector(pageViewItems:)]) {
        self.pageItems = [self.dataSource pageViewItems:self];
    }
    
    if (self.pageItems) {
        [self.collectionView reloadData];
    }
}

- (void)resetContentOffsetAnimated:(BOOL)animated {
    if (self.pageItems.count > 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        [self.collectionView selectItemAtIndexPath:indexPath animated:animated scrollPosition:UICollectionViewScrollPositionLeft];
    }
}

#pragma mark - Private methods

- (NSString *)reuseIdentifierWithCellClass:(Class)cellClass {
    return [NSString stringWithFormat:@"%@-%@", @"pageView", NSStringFromClass(cellClass)];
}

- (void)registerCell:(Class)cellClass {
    NSString *identifier = [self reuseIdentifierWithCellClass:cellClass];
    [self.collectionView registerClass:cellClass forCellWithReuseIdentifier:identifier];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.pageItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [self reuseIdentifierWithCellClass:_cellClass];
    IMOPageViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.delegate = self;
    [cell fillCellWithItem:self.pageItems[indexPath.item] atIndexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(pageView:didSelectItemAtIndex:)]) {
        [self.delegate pageView:self didSelectItemAtIndex:indexPath.row];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(pageView:scrollViewDidScroll:)]) {
        [self.delegate pageView:self scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(pageView:scrollViewDidEndDecelerating:)]) {
        [self.delegate pageView:self scrollViewDidEndDecelerating:scrollView];
    }
}

#pragma mark - IMOPageViewCellDelegate

- (void)didTapPageCell:(IMOPageViewCell *)pageCell {
    if ([self.delegate respondsToSelector:@selector(pageView:didTapCellAtIndex:)]) {
        NSInteger index = [[self.collectionView indexPathForCell:pageCell] item];
        [self.delegate pageView:self didTapCellAtIndex:index];
    }
}

#pragma mark - Getter and setter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewLayout *layout = [[UICollectionViewLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor collectionViewBackgroundColor];  
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.scrollsToTop = NO;
        _collectionView.pagingEnabled = YES;
        _collectionView.delegate = self;
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

@end

//
//  IMOTextView.m
//
//  Created by spemagic on 10/1/14.
//  Copyright (c) 2014 spemagic. All rights reserved.
//

#import "IMOTextView.h"

static const NSUInteger kTextViewMaxLinesIniPhone = 4;

NSString *const IMOTextViewContentSizeDidChangeNotification = @"IMOTextViewContentSizeDidChangeNotification";

@interface IMOTextView ()

// The initial font point size, used for dynamic type calculations
@property (nonatomic) CGFloat initialFontSize;

// The label used as placeholder
@property (nonatomic, strong) UILabel *placeholderLabel;

// Used for detecting if the scroll indicator was previously flashed
@property (nonatomic) BOOL didFlashScrollIndicators;

@end

@implementation IMOTextView

#pragma mark - View lifecycle

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp
{
    self.editable = YES;
    self.selectable = YES;
    self.directionalLockEnabled = YES;
    self.dataDetectorTypes = UIDataDetectorTypeNone;
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = YES;
    self.scrollEnabled = YES;
    self.scrollsToTop = NO;
    self.userInteractionEnabled = YES;
    self.textColor = [UIColor blackColor];
    self.backgroundColor = [UIColor clearColor];
    self.enablesReturnKeyAutomatically = YES;
    self.keyboardAppearance = UIKeyboardAppearanceDefault;
    self.keyboardType = UIKeyboardTypeDefault;
    self.returnKeyType = UIReturnKeySend;
    self.textAlignment = NSTextAlignmentLeft;
    self.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.font = [UIFont systemFontOfSize:16];
    
    CGFloat inset = -3;
    self.scrollIndicatorInsets = UIEdgeInsetsMake(inset, 0, inset, 0);
    self.contentInset = UIEdgeInsetsMake(inset, 0, inset, 0);
    
    self.maxNumberOfLines = kTextViewMaxLinesIniPhone;
    
    [self addObserver:self forKeyPath:NSStringFromSelector(@selector(contentSize)) options:NSKeyValueObservingOptionNew context:NULL];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(slk_didChangeText:) name:UITextViewTextDidChangeNotification object:nil];
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(contentSize))];
    
     [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:nil];
    
    _placeholderLabel = nil;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.placeholderLabel.hidden = [self slk_shouldHidePlaceholder];
    
    if (!self.placeholderLabel.hidden) {
        
        [UIView performWithoutAnimation:^{
            self.placeholderLabel.frame = [self slk_placeholderRectThatFits:self.bounds];
            [self sendSubviewToBack:self.placeholderLabel];
        }];
    }
}

#pragma mark - Praivate methods

- (CGSize)intrinsicContentSize
{
    return CGSizeMake(UIViewNoIntrinsicMetric, kTextViewIntrinsicHeight);
}

- (CGFloat)contentVerticalInset
{
    return self.intrinsicContentSize.height - self.font.lineHeight;
}

- (NSUInteger)numberOfLines
{
    
    CGRect textFrame=[[self layoutManager]usedRectForTextContainer:[self textContainer]];

    return fabs(textFrame.size.height / self.font.lineHeight);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isEqual:self] && [keyPath isEqualToString:NSStringFromSelector(@selector(contentSize))]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:IMOTextViewContentSizeDidChangeNotification object:self userInfo:nil];
    }
}

- (void)slk_didChangeText:(NSNotification *)notification
{
    if (![notification.object isEqual:self]) {
        return;
    }
    
    if (self.placeholderLabel.hidden != [self slk_shouldHidePlaceholder]) {
        [self setNeedsLayout];
    }
    
    [self slk_flashScrollIndicatorsIfNeeded];
}

- (void)slk_flashScrollIndicatorsIfNeeded
{
    if (self.numberOfLines == self.maxNumberOfLines+1) {
        if (!_didFlashScrollIndicators) {
            _didFlashScrollIndicators = YES;
            [super flashScrollIndicators];
        }
    }
    else if (_didFlashScrollIndicators) {
        _didFlashScrollIndicators = NO;
    }
}

- (BOOL)slk_shouldHidePlaceholder
{
    if (self.placeholder.length == 0 || self.text.length > 0) {
        return YES;
    }
    return NO;
}

- (CGRect)slk_placeholderRectThatFits:(CGRect)bounds
{
    CGRect rect = CGRectZero;
    rect.size = [self.placeholderLabel sizeThatFits:bounds.size];
    rect.origin = UIEdgeInsetsInsetRect(bounds, self.textContainerInset).origin;
    
    CGFloat padding = self.textContainer.lineFragmentPadding;
    rect.origin.x += padding;
    rect.origin.y -= bounds.origin.y;
    return rect;
}

+ (CGFloat)pointSizeDifferenceForCategory:(NSString *)category
{
    if ([category isEqualToString:UIContentSizeCategoryExtraSmall])                         return -3.0;
    if ([category isEqualToString:UIContentSizeCategorySmall])                              return -2.0;
    if ([category isEqualToString:UIContentSizeCategoryMedium])                             return -1.0;
    if ([category isEqualToString:UIContentSizeCategoryLarge])                              return 0.0;
    if ([category isEqualToString:UIContentSizeCategoryExtraLarge])                         return 2.0;
    if ([category isEqualToString:UIContentSizeCategoryExtraExtraLarge])                    return 4.0;
    if ([category isEqualToString:UIContentSizeCategoryExtraExtraExtraLarge])               return 6.0;
    if ([category isEqualToString:UIContentSizeCategoryAccessibilityMedium])                return 8.0;
    if ([category isEqualToString:UIContentSizeCategoryAccessibilityLarge])                 return 10.0;
    if ([category isEqualToString:UIContentSizeCategoryAccessibilityExtraLarge])            return 11.0;
    if ([category isEqualToString:UIContentSizeCategoryAccessibilityExtraExtraLarge])       return 12.0;
    if ([category isEqualToString:UIContentSizeCategoryAccessibilityExtraExtraExtraLarge])  return 13.0;
    return 0;
}


#pragma mark - Getters

- (UILabel *)placeholderLabel
{
    if (!_placeholderLabel) {
        _placeholderLabel = [UILabel new];
        _placeholderLabel.clipsToBounds = NO;
        _placeholderLabel.autoresizesSubviews = NO;
        _placeholderLabel.numberOfLines = 1;
        _placeholderLabel.font = self.font;
        _placeholderLabel.backgroundColor = [UIColor clearColor];
        _placeholderLabel.textColor = [UIColor lightGrayColor];
        _placeholderLabel.hidden = YES;
        
        [self addSubview:_placeholderLabel];
    }
    return _placeholderLabel;
}

- (NSString *)placeholder
{
    return self.placeholderLabel.text;
}

- (UIColor *)placeholderColor
{
    return self.placeholderLabel.textColor;
}

#pragma mark - Setters
- (void)setPlaceholder:(NSString *)placeholder
{
    self.placeholderLabel.text = placeholder;
    self.accessibilityLabel = placeholder;
    
    [self setNeedsLayout];
}

- (void)setPlaceholderColor:(UIColor *)color
{
    self.placeholderLabel.textColor = color;
}

- (void)setFont:(UIFont *)font
{
    NSString *contentSizeCategory = [[UIApplication sharedApplication] preferredContentSizeCategory];
    
    [self setFontName:font.familyName pointSize:font.pointSize withContentSizeCategory:contentSizeCategory];
    
    self.initialFontSize = font.pointSize;
}

- (void)setFontName:(NSString *)fontName pointSize:(CGFloat)pointSize withContentSizeCategory:(NSString *)contentSizeCategory
{
    pointSize += [IMOTextView pointSizeDifferenceForCategory:contentSizeCategory];
    
    UIFont *dynamicFont = [UIFont fontWithName:fontName size:pointSize];
    
    [super setFont:dynamicFont];
    
    // Updates the placeholder font too
    self.placeholderLabel.font = dynamicFont;
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment
{
    [super setTextAlignment:textAlignment];
    
    // Updates the placeholder text alignment too
    self.placeholderLabel.textAlignment = textAlignment;
}


@end

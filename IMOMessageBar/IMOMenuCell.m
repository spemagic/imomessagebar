//
//  IMOMenuCell.m
//  imoffice
//
//  Created by zhanghao on 15/7/6.
//  Copyright (c) 2015年 IMO. All rights reserved.
//

#import "IMOMenuCell.h"
#import "IMOMacros.h"
#import "UIView+IMOAutoLayout.h"
#import "UIColor+IMOColor.h"
#import "IMOGlobalQueue.h"

static const CGFloat kDefaultMenuButtonWidth = 59;
static const CGFloat kDefaultMenuButtonBottomMargin = 5;
static const CGFloat kDefaultMenuTitleHeight = 16;

@interface IMOMenuCell ()

@property (nonatomic, strong, readwrite) UIButton *menuButton;
@property (nonatomic, strong, readwrite) UILabel *menuTitleLabel;

@end

@implementation IMOMenuCell

#pragma mark - View lifecycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self.menuTitleLabel addMask:IMOAutoLayoutMaskHeightEqual constant:kDefaultMenuTitleHeight];
        [self.menuTitleLabel addMask:IMOAutoLayoutMaskLeading|IMOAutoLayoutMaskTrailing|IMOAutoLayoutMaskBottom];

        [self.menuButton addMask:IMOAutoLayoutMaskCenterX];
        [self.menuButton addMask:IMOAutoLayoutMaskWidthEqual constant:kDefaultMenuButtonWidth];
        [self.menuButton addMask:IMOAutoLayoutMaskHeightEqual constant:kDefaultMenuButtonWidth];
        
        [self.menuButton addMask:IMOAutoLayoutMaskBottom toView:self.menuTitleLabel constant:-(kDefaultMenuButtonBottomMargin+kDefaultMenuTitleHeight)];
    }
    return self;
}

#pragma mark - Override methods

- (void)fillCellWithItem:(IMOPageItem *)item atIndexPath:(NSIndexPath *)indexPath {
    self.menuTitleLabel.text = item.title;
    
    dispatch_async_resource_queue((^{
        // Fix a Xcode console warning : http://stackoverflow.com/questions/22011106/error-cuicatalog-invalid-asset-name-supplied-null-or-invalid-scale-factor
        UIImage *normalImage = nil;
        if (item.normalName) {
            normalImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@", item.normalName]];
        }
        UIImage *highlightImage = nil;
        if (item.highlightName) {
            highlightImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@", item.highlightName]];
        }
        dispatch_main_async_safe(^{
            if (normalImage) {
                [self.menuButton setImage:normalImage forState:UIControlStateNormal];
            }
            if (highlightImage) {
                [self.menuButton setImage:highlightImage forState:UIControlStateHighlighted];
            }
        });
    }));
}

#pragma mark - Actions

- (void)menuButtonTapped:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(didTapPageCell:)]) {
        [self.delegate didTapPageCell:self];
    }
}

#pragma mark - Getter and setter

- (UIButton *)menuButton {
    if (!_menuButton) {
        _menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_menuButton addTarget:self action:@selector(menuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_menuButton];
    }
    return _menuButton;
}

- (UILabel *)menuTitleLabel {
    if (!_menuTitleLabel) {
        _menuTitleLabel = [UILabel new];
        _menuTitleLabel.textAlignment = NSTextAlignmentCenter;
        _menuTitleLabel.textColor = RGB(136, 136, 136);
        _menuTitleLabel.font = [UIFont systemFontOfSize:13];
        [self.contentView addSubview:_menuTitleLabel];
    }
    return _menuTitleLabel;
}

@end

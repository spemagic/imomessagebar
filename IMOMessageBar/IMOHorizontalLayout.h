//
//  IMOHorizontalLayout.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/7.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMOHorizontalLayout : UICollectionViewLayout

// 旋转时是否更新itemSize，默认为NO
@property (nonatomic, assign) BOOL updateItemSize;

- (instancetype)initWithItemWidth:(CGFloat)itemWidth;

@end

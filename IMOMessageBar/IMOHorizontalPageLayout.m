//
//  IMOHorizontalPageLayout.m
//  imoffice
//
//  Created by zhanghao on 15/7/6.
//  Copyright (c) 2015年 IMO. All rights reserved.
//

#import "IMOHorizontalPageLayout.h"
#import "IMOMacros.h"

@interface IMOHorizontalPageLayout ()

@property (nonatomic, copy) NSArray *layoutAttributes;
@property (nonatomic, assign) CGSize itemSize;

@end

@implementation IMOHorizontalPageLayout

#pragma mark - Object lifecycle

- (instancetype)init {
    self = [super init];
    if (self) {
        _numberOfRowsInLandscape = 1;
        _numberOfRowsInPortrait = 1;
        _numberOfColumnInLandscape = 1;
        _numberOfColumnInPortrait = 1;
    }
    return self;
}

#pragma mark - Getter

- (NSInteger)numberOfRows {
    return IMOInterfaceOrientationIsLandscape ? self.numberOfRowsInLandscape : self.numberOfRowsInPortrait;
}

- (NSInteger)numberOfColumn {
    return IMOInterfaceOrientationIsLandscape ? self.numberOfColumnInLandscape : self.numberOfColumnInPortrait;
}

#pragma mark - Override layout methods

- (void)prepareLayout {
    CGFloat width = CGRectGetWidth(self.collectionView.bounds) / self.numberOfColumn;
    CGFloat height = CGRectGetHeight(self.collectionView.bounds) / self.numberOfRows;
    self.itemSize = CGSizeMake(width, height);

    NSMutableArray *layoutAttributesArray = [NSMutableArray array];
    
    NSInteger sections = [self.collectionView numberOfSections];
    for (NSInteger section = 0; section < sections; section++) {
        NSInteger items = [self.collectionView numberOfItemsInSection:section];
        
        for (NSInteger item = 0; item < items; item++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            UICollectionViewLayoutAttributes *attrbute = [self layoutAttributesForItemAtIndexPath:indexPath];
            [layoutAttributesArray addObject:attrbute];
        }
    }
    
    self.layoutAttributes = [NSArray arrayWithArray:layoutAttributesArray];
}

- (CGSize)collectionViewContentSize {
    NSInteger pages = ceil(self.layoutAttributes.count / (CGFloat)(self.numberOfRows * self.numberOfColumn));
    CGSize size = CGSizeMake(CGRectGetWidth(self.collectionView.bounds) * pages, CGRectGetHeight(self.collectionView.bounds));
    return size;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    return self.layoutAttributes;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return NO;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes *attrbute = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];

    NSInteger column = indexPath.item % self.numberOfColumn;
    NSInteger line = indexPath.item / self.numberOfColumn % self.numberOfRows;
    NSInteger page = indexPath.item / (self.numberOfRows * self.numberOfColumn);
    
    CGRect frame = CGRectMake(column * self.itemSize.width + CGRectGetWidth(self.collectionView.bounds) * page, line * self.itemSize.height, self.itemSize.width, self.itemSize.height);
    attrbute.frame = frame;
    attrbute.zIndex = indexPath.item;
    return attrbute;
}

@end

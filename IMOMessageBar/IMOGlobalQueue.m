//
//  IMOGlobalQueue.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/10/13.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "IMOGlobalQueue.h"

@implementation IMOGlobalQueue

#pragma mark - Global queue

dispatch_queue_t imo_async_load_resource_queue() {
    static dispatch_queue_t imo_resource_queue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        imo_resource_queue = dispatch_queue_create("com.imoffice.resource.queue", NULL);
    });
    return imo_resource_queue;
}

@end

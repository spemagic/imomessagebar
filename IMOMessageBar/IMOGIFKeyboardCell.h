//
//  IMOGIFKeyboardCell.h
//  IMOMessageBar
//
//  Created by spemagic on 10/13/15.
//  Copyright © 2015 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMOEmojiModel.h"

@protocol IMOGIFKeyboardCellDelegate;

@interface IMOGIFKeyboardCell : UICollectionViewCell

@property (nonatomic, weak) id<IMOGIFKeyboardCellDelegate> delegate;
@property (nonatomic, strong) IMOEmojiModel *emojiModel;

@property (nonatomic, readonly) UICollectionView *collectionView;

@end

@protocol IMOGIFKeyboardCellDelegate <NSObject>

@optional
- (void)gifCell:(IMOGIFKeyboardCell *)cell didTapGIFAtIndex:(NSInteger)index;

@end
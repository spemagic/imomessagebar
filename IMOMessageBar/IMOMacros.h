//
//  IMOMacros.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/9/4.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#ifndef IMOMessageBar_IMOMacros_h
#define IMOMessageBar_IMOMacros_h

#define dispatch_main_async_safe(_block_)\
    if ([NSThread isMainThread]) {\
        _block_();\
    } else {\
        dispatch_async(dispatch_get_main_queue(), _block_);\
    }

#define dispatch_async_resource_queue(block__)\
    dispatch_async(imo_async_load_resource_queue(), block__)

#define dispatch_async_high_queue(block__)\
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), block__)

#define dispatch_async_default_queue(block__)\
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block__)

#define IMOThrowExcptionWithReason(_reason_)\
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:_reason_ userInfo:nil];

/**
 Raises an `NSInternalInconsistencyException` if the `condition` does not pass.
 Use `description` to supply the way to fix the exception.
 */
#define IMOConsistencyAssert(condition, description, ...) \
    do { \
        if (!(condition)) { \
            [NSException raise:NSInternalInconsistencyException \
                        format:description, ##__VA_ARGS__]; \
        } \
    } while(0)

/**
 Always raises `NSInternalInconsistencyException` with details
 about the method used and class that received the message
 */
#define IMONotDesignatedInitializer() \
    do { \
        IMOConsistencyAssert(NO, \
                            @"%@ is not the designated initializer for instances of %@.", \
                            NSStringFromSelector(_cmd), \
                            NSStringFromClass([self class])); \
        return nil; \
    } while (0)

#define RGB(r, g, b)        RGBA(r, g, b, 1)
#define RGBA(r, g, b, a)    [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

#define IMOInterfaceOrientationIsPortrait   UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])

#define IMOInterfaceOrientationIsLandscape  UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])

#define kIsiPhone6           ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) && [[UIScreen mainScreen] bounds].size.width == 375.0)
#define kIsiPhone6P          ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) && [[UIScreen mainScreen] bounds].size.width == 414.0)

#define kIsiPhone6InLandscape           ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) && [[UIScreen mainScreen] bounds].size.height == 375.0)
#define kIsiPhone6PInLandscape          ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) && [[UIScreen mainScreen] bounds].size.height == 414.0)

#endif

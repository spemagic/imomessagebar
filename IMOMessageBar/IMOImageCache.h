//
//  IMOImageCache.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/10/14.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMOImageCache : NSObject

- (void)storeImage:(UIImage *)image forKey:(NSString *)key;
- (UIImage *)queryImageWithKey:(NSString *)key;
- (void)removeAllCaches;

@end

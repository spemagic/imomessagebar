//
//  IMOEmojiKeyboardToolBar.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/7.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IMOEmojiKeyboardToolBarDelegate;

@interface IMOEmojiKeyboardToolBar : UIView

@property (nonatomic, copy) NSArray *emojiBarButtonNames;
@property (nonatomic, assign) BOOL enableSend;

@property (nonatomic, weak) id<IMOEmojiKeyboardToolBarDelegate> delegate;

@property (nonatomic, readonly) UICollectionView *collectionView;

- (void)showSendButton:(BOOL)show animated:(BOOL)animated;

@end

@protocol IMOEmojiKeyboardToolBarDelegate <NSObject>

@optional
- (void)emojiToolBar:(IMOEmojiKeyboardToolBar *)emojiToolBar didTapBarButtonIndex:(NSUInteger)index isSameIndex:(BOOL)isSameIndex;
- (void)emojiToolBar:(IMOEmojiKeyboardToolBar *)emojiToolBar didTapSendButton:(UIButton *)sendButton;

@end
//
//  IMOGlobalQueue.h
//  IMOMessageBar
//
//  Created by zhanghao on 15/10/13.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMOGlobalQueue : NSObject

dispatch_queue_t imo_async_load_resource_queue();

@end

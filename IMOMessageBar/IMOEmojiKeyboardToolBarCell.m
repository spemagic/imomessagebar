//
//  IMOEmojiKeyboardToolBarCell.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/7.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "IMOEmojiKeyboardToolBarCell.h"
#import "IMOGlobalQueue.h"
#import "UIView+IMOAutoLayout.h"
#import "UIColor+IMOColor.h"
#import "IMOMacros.h"

@interface IMOEmojiKeyboardToolBarCell ()

@property (nonatomic, strong, readwrite) UIImageView *emojiBarButtonImageView;
@property (nonatomic, strong) UIView *trailingSeperatorView;

@end

@implementation IMOEmojiKeyboardToolBarCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];

        [self.emojiBarButtonImageView addMask:IMOAutoLayoutMaskCenterX];
        [self.emojiBarButtonImageView addMask:IMOAutoLayoutMaskCenterY];
        [self.emojiBarButtonImageView addMask:IMOAutoLayoutMaskWidthEqual constant:24];
        [self.emojiBarButtonImageView addMask:IMOAutoLayoutMaskHeightEqual constant:24];
        
        NSDictionary *metrics = @{ @"seperatorWidth": @(0.2),
                                   @"seperatorTopMargin": @(5)
                                   };
        NSDictionary *views = @{ @"trailingSeperatorView": self.trailingSeperatorView };
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[trailingSeperatorView(seperatorWidth)]|" options:0 metrics:metrics views:views]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(seperatorTopMargin)-[trailingSeperatorView]-(seperatorTopMargin)-|" options:0 metrics:metrics views:views]];
    }
    return self;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    self.contentView.backgroundColor = selected ? [UIColor collectionViewBackgroundColor] : [UIColor whiteColor];
}

- (void)fillCellWithEmojiBarButtonName:(NSString *)name {
    dispatch_async_resource_queue((^{
        UIImage *image = [UIImage imageNamed:name];
        dispatch_main_async_safe(^{
            self.emojiBarButtonImageView.image = image;
        });
    }));
}

- (UIImageView *)emojiBarButtonImageView {
    if (!_emojiBarButtonImageView) {
        _emojiBarButtonImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_emojiBarButtonImageView];
    }
    return _emojiBarButtonImageView;
}

- (UIView *)trailingSeperatorView {
    if (!_trailingSeperatorView) {
        _trailingSeperatorView = [UIView new];
        _trailingSeperatorView.translatesAutoresizingMaskIntoConstraints = NO;
        _trailingSeperatorView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
        [self.contentView addSubview:_trailingSeperatorView];
    }
    return _trailingSeperatorView;
}

@end

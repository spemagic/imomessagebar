//
//  IMOInputBar.m
//  IMOMessageBar
//
//  Created by spemagic on 7/7/15.
//  Copyright (c) 2015 zhanghao. All rights reserved.
//

#import "IMOInputBar.h"
#import "UIView+IMOAutoLayout.h"
#import "UIColor+IMOColor.h"
#import "UIImage+IMOImage.h"
#import "IMOMacros.h"
#import "IMOEmojiKeyboard.h"
#import "IMOMenuPicker.h"
#import "IMOPageItem.h"

const CGFloat kEmojiKeyboardDefaultHeight = 216;
const CGFloat kMessageInputBarNormalHeight = 50;

@interface IMOInputBar () <IMOEmojiKeyboardDataSource, IMOEmojiKeyboardDelegate, IMOMenuPickerDelegate, UITextViewDelegate> {
    CGFloat _inputBarHeight;
    CGFloat _menuPickerHeight;
}

@property (nonatomic, strong, readwrite) UITextView *textView;
@property (nonatomic, strong) UIView *topSeperatorView;
@property (nonatomic, strong) UIButton *voiceButton;
@property (nonatomic, strong) UIButton *emojiButton;
@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) UIButton *talkButton;
@property (nonatomic, strong) IMOEmojiKeyboard *emojiKeyboard;
@property (nonatomic, strong) IMOMenuPicker *menuPicker;

@property (nonatomic, assign) IMOInputBarInteractionMode interactionMode;

@property (nonatomic, copy) NSArray *emojiNames;

@end

@implementation IMOInputBar

#pragma mark - View lifecycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    self.backgroundColor = RGB(241, 241, 241);

    _inputBarHeight = kMessageInputBarNormalHeight;
    _menuPickerHeight = kEmojiKeyboardDefaultHeight;
    
    self.emojiNames = [IMOEmojiViewModel getAllEmojiNamesWithPortraitMode:YES];
    
    [self switchToInteractionMode:IMOInputBarInteractionModeNormal];
    [self registerNotifications];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat width = CGRectGetWidth(self.bounds);
    
    CGRect topSeperatorFrame = self.topSeperatorView.frame;
    topSeperatorFrame.size.width = width;
    self.topSeperatorView.frame = topSeperatorFrame;
    
    CGFloat buttonWidth = 32;
    CGFloat buttonTop = (kMessageInputBarNormalHeight - buttonWidth) / 2;
    CGFloat buttonTrailing = buttonTop;
    self.voiceButton.frame = CGRectMake(5, buttonTop, buttonWidth, buttonWidth);
    self.menuButton.frame = CGRectMake(width - buttonWidth - buttonTrailing, buttonTop, buttonWidth, buttonWidth);
    self.emojiButton.frame = CGRectMake(CGRectGetMinX(self.menuButton.frame) - buttonWidth - buttonTrailing, buttonTop, buttonWidth, buttonWidth);
    
    CGFloat textViewTop = 6;
    CGFloat textViewLeadding = 8;
    CGFloat textViewWidth = CGRectGetMinX(self.emojiButton.frame) - textViewLeadding * 2 - CGRectGetMaxX(self.voiceButton.frame);
    self.textView.frame = CGRectMake(CGRectGetMaxX(self.voiceButton.frame) + textViewLeadding, textViewTop, textViewWidth, _inputBarHeight - textViewTop * 2);
    self.talkButton.frame = self.textView.frame;
}

#pragma mark - Notifications

- (void)registerNotifications {
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)keyboardWillChangeFrame:(NSNotification *)notification {
    if (self.interactionMode == IMOInputBarInteractionModeMenu ||
        self.interactionMode == IMOInputBarInteractionModeEmoji) {
        return;
    }

    NSDictionary *userInfo = notification.userInfo;
    CGRect endFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    UIViewAnimationOptions options = ((curve << 16) | UIViewAnimationOptionBeginFromCurrentState);

    CGRect frame = self.frame;
    frame.size.height = _inputBarHeight;
    frame.origin.y = CGRectGetMinY(endFrame) - _inputBarHeight;
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:options
                     animations:^{
                         self.frame = frame;
                     } completion:^(BOOL finished) {
                         if (self.emojiKeyboard.superview) {
                             [self.emojiKeyboard removeFromSuperview];
                         }
                         if (self.menuPicker.superview) {
                             [self.menuPicker removeFromSuperview];
                         }
                     }];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    if (self.interactionMode == IMOInputBarInteractionModeMenu ||
        self.interactionMode == IMOInputBarInteractionModeEmoji) {
        return;
    }
    
    NSDictionary *userInfo = notification.userInfo;
    CGRect endFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    UIViewAnimationOptions options = ((curve << 16) | UIViewAnimationOptionBeginFromCurrentState);
    
//    CGFloat keyboardY = CGRectGetMinY([self convertRect:endFrame fromView:nil]);
    CGFloat keyboardY = CGRectGetMinY(endFrame);
    
    CGRect frame = self.frame;
    frame.size.height = _inputBarHeight;
    frame.origin.y = keyboardY - _inputBarHeight;
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:options
                     animations:^{
                         self.frame = frame;
                     } completion:^(BOOL finished) {
                         if (self.emojiKeyboard.superview) {
                             [self.emojiKeyboard removeFromSuperview];
                         }
                         if (self.menuPicker.superview) {
                             [self.menuPicker removeFromSuperview];
                         }
                     }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    if (self.interactionMode == IMOInputBarInteractionModeMenu ||
        self.interactionMode == IMOInputBarInteractionModeEmoji) {
        return;
    }
    
    NSDictionary *userInfo = notification.userInfo;
    CGRect endFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    UIViewAnimationOptions options = ((curve << 16) | UIViewAnimationOptionBeginFromCurrentState);
    
//    CGFloat keyboardY = CGRectGetMinY([self convertRect:endFrame fromView:nil]);
    CGFloat keyboardY = CGRectGetMinY(endFrame);
    
    CGRect frame = self.frame;
    frame.size.height = _inputBarHeight;
    frame.origin.y = keyboardY - _inputBarHeight;
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:options
                     animations:^{
                         self.frame = frame;
                     } completion:^(BOOL finished) {
                         if (self.emojiKeyboard.superview) {
                             [self.emojiKeyboard removeFromSuperview];
                         }
                         if (self.menuPicker.superview) {
                             [self.menuPicker removeFromSuperview];
                         }
                     }];
}

#pragma mark - Interaction mode switch

- (void)switchToInteractionMode:(IMOInputBarInteractionMode)mode animated:(BOOL)animated {
    self.interactionMode = mode;
    if (mode != IMOInputBarInteractionModeInput) {
        [self.textView resignFirstResponder];
    }
    
    BOOL showTalkButton = NO;
    BOOL menuToEmoji = NO;
    BOOL emojiToMenu = NO;
    
    CGRect inputBarFrame = self.frame;
    CGFloat superViewHeight = CGRectGetHeight(self.superview.bounds);
    
    switch (mode) {
        case IMOInputBarInteractionModeStopInput: {
            showTalkButton = !self.talkButton.isHidden;
            self.emojiButton.selected = NO;
            self.menuButton.selected = NO;
            
            inputBarFrame.origin.y = superViewHeight - _inputBarHeight;
            inputBarFrame.size.height = _inputBarHeight;
        }
            break;
        case IMOInputBarInteractionModeVoice: {
            showTalkButton = YES;
            self.emojiButton.selected = NO;
            self.menuButton.selected = NO;
            
            inputBarFrame.origin.y = superViewHeight - _inputBarHeight;
            inputBarFrame.size.height = _inputBarHeight;
        }
            break;
        case IMOInputBarInteractionModeEmoji: {
            showTalkButton = NO;
            
            if (!self.emojiKeyboard.superview) {
                [self addSubview:self.emojiKeyboard];
            }
            // 如果是从菜单栏切换到富文本表情栏
            if (self.menuButton.isSelected) {
                menuToEmoji = YES;
                CGRect initFrame = self.emojiKeyboard.frame;
                initFrame.origin.y = superViewHeight;
                self.emojiKeyboard.frame = initFrame;
            }
            
            CGFloat inputBarHeight = _inputBarHeight + kEmojiKeyboardDefaultHeight;
            inputBarFrame.origin.y = superViewHeight - inputBarHeight;
            inputBarFrame.size.height = inputBarHeight;
            
            self.voiceButton.selected = NO;
            self.menuButton.selected = NO;
        }
            break;
        case IMOInputBarInteractionModeMenu: {
            showTalkButton = NO;
            
            if (!self.menuPicker.superview) {
                [self addSubview:self.menuPicker];
            }
            // 如果是从富文本表情栏切换到菜单栏
            if (self.emojiButton.isSelected) {
                emojiToMenu = YES;
                CGRect initFrame = self.menuPicker.frame;
                initFrame.origin.y = superViewHeight;
                self.menuPicker.frame = initFrame;
            }
            [self.emojiKeyboard removeFromSuperview];
            
            CGFloat inputBarHeight = _inputBarHeight + _menuPickerHeight;
            inputBarFrame.origin.y = superViewHeight - inputBarHeight;
            inputBarFrame.size.height = inputBarHeight;
            
            self.voiceButton.selected = NO;
            self.emojiButton.selected = NO;
        }
            break;
        case IMOInputBarInteractionModeNormal: {
            showTalkButton = NO;
            self.voiceButton.selected = NO;
            self.emojiButton.selected = NO;
            self.menuButton.selected = NO;
        }
            break;
        case IMOInputBarInteractionModeInput: {
            showTalkButton = NO;
            self.voiceButton.selected = NO;
            self.emojiButton.selected = NO;
            self.menuButton.selected = NO;
        }
            break;
        default:
            break;
    }
    
    void(^animations)() = ^{
        self.talkButton.hidden = !showTalkButton;
        self.textView.hidden = showTalkButton;
        self.frame = inputBarFrame;
        
        if (menuToEmoji) {
            CGRect emojiKeyboardFrame = self.emojiKeyboard.frame;
            emojiKeyboardFrame.origin.y = _inputBarHeight;
            self.emojiKeyboard.frame = emojiKeyboardFrame;
        }
        if (emojiToMenu) {
            CGRect menuPickerFrame = self.menuPicker.frame;
            menuPickerFrame.origin.y = _inputBarHeight;
            self.menuPicker.frame = menuPickerFrame;
        }
    };
    
    void(^completion)(BOOL) = ^(BOOL finished) {
        if (menuToEmoji && !self.menuButton.isSelected) {
            [self.menuPicker removeFromSuperview];
        } else {
            if (mode != IMOInputBarInteractionModeInput && mode != IMOInputBarInteractionModeEmoji) {
                if (self.emojiKeyboard.superview && !self.emojiButton.isSelected) {
                    [self.emojiKeyboard removeFromSuperview];
                }
            }
            if (mode != IMOInputBarInteractionModeInput && mode != IMOInputBarInteractionModeMenu) {
                if (self.menuPicker.superview && !self.menuButton.isSelected) {
                    [self.menuPicker removeFromSuperview];
                }
            }
        }
    };
    
    UIViewAnimationOptions options = (7 << 16 | UIViewAnimationOptionBeginFromCurrentState);
    [UIView animateWithDuration:animated ? 0.25 : 0
                          delay:0
                        options:options
                     animations:animations
                     completion:completion];
}

- (void)switchToInteractionMode:(IMOInputBarInteractionMode)mode {
    [self switchToInteractionMode:mode animated:NO];
}

#pragma mark - Actions

- (void)voiceButtonAction:(UIButton *)button {
    [self didTapButton:button];
}

- (void)emojiButtonAction:(UIButton *)button {
    [self didTapButton:button];
}

- (void)menuButtonAction:(UIButton *)button {
    [self didTapButton:button];
}

- (void)didTapButton:(UIButton *)button {
    button.selected = !button.selected;
    
    IMOInputBarInteractionMode mode = IMOInputBarInteractionModeUnknown;
    IMOInputBarButton buttonType = IMOInputBarButtonUnknown;
    
    if (button == self.voiceButton) {
        buttonType = IMOInputBarButtonVoice;
        mode = button.selected ? IMOInputBarInteractionModeVoice : IMOInputBarInteractionModeInput;
    } else if (button == self.emojiButton) {
        buttonType = IMOInputBarButtonEmoji;
        mode = button.selected ? IMOInputBarInteractionModeEmoji : IMOInputBarInteractionModeInput;
    } else if (button == self.menuButton) {
        buttonType = IMOInputBarButtonMenu;
        mode = button.selected ? IMOInputBarInteractionModeMenu : IMOInputBarInteractionModeInput;
    }
    [self switchToInteractionMode:mode animated:YES];
    
    if (mode == IMOInputBarInteractionModeInput) {
        [self.textView becomeFirstResponder];
    }
    
    if ([self.delegate respondsToSelector:@selector(inputBar:didTapButton:)]) {
        [self.delegate inputBar:self didTapButton:buttonType];
    }
}

- (void)talkButtonTouchDown:(UIButton *)button {
    
}

- (void)talkButtonTouchUpOutside:(UIButton *)button {
    
}

- (void)talkButtonTouchUpInside:(UIButton *)button {
    
}

- (void)talkButtonTouchDragExit:(UIButton *)button {
    
}

- (void)talkButtonTouchDragEnter:(UIButton *)button {
    
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [self switchToInteractionMode:IMOInputBarInteractionModeInput animated:YES];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
//        NSString *originText = textView.text;
//        if ([self.delegate respondsToSelector:@selector(inputBar:textViewDidSendText:)]) {
//            [self.delegate inputBar:self textViewDidSendText:originText];
            return NO;
//        }
//        return YES;
    } else {
//        if ([self.delegate respondsToSelector:@selector(inputBar:shouldChangeTextInRange:replacementText:)]) {
//            return [self.delegate inputBar:self shouldChangeTextInRange:range replacementText:text];
//        } else {
            return YES;
//        }
    }
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    return YES;
}

#pragma mark - IMOEmojiKeyboardDataSource

- (NSArray *)bottomToolBarImageNamesInEmojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard {
    return @[@"emoji_tool_bar_button_emoji", @"ban_che_xiao_zi"];
}

- (NSArray *)emojiPickerImageNamesInEmojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard {
    return self.emojiNames;
}

#pragma mark - IMOEmojiKeyboardDelegate

- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard didTapEmojiAtIndexPath:(NSIndexPath *)indexPath subIndex:(NSInteger)subIndex {
    IMOEmojiModel *emojiModel = self.emojiNames[indexPath.section][indexPath.row];
    NSString *emojiName = emojiModel.emojiNames[subIndex];
    
    if (emojiModel.isGIF) {
        
    } else {
        if ([emojiName isEqualToString:kEmojiPickerDeleteButtonKey]) {
//            NSString *text = self.textView.text;
//            if (text.length) {
//                NSString *lastCharacter = [text substringFromIndex:text.length - 1];
//
//                if ([lastCharacter isEqualToString:@"]"]) {
//                    NSArray *allEmotions = [self.regularExpression matchesInString:text
//                                                                           options:NSMatchingWithTransparentBounds
//                                                                             range:NSMakeRange(0, text.length)];
//                    if (allEmotions.count) {
//                        NSRange range = [allEmotions.lastObject range];
//                        NSString *emotion = [text substringWithRange:range];
//                        text = [text substringToIndex:text.length - emotion.length];
//                    } else {
//                        text = [text substringToIndex:text.length - 1];
//                    }
//                } else {
//                    text = [text substringToIndex:text.length - 1];
//                }
//            }
//
//            self.textView.text = text;
        } else {
            NSString *appending = [NSString stringWithFormat:@"[%@]", emojiName];
            self.textView.text = [self.textView.text stringByAppendingString:appending];
            [emojiKeyboard enableSendButton:YES];
        }
    }
}

- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard didTapSendButton:(UIButton *)sendButton {
//    NSString *sendText = self.textView.text;
    [emojiKeyboard enableSendButton:NO];
    self.textView.text = nil;
}

- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard didTapEmojiBarButtonAtIndex:(NSInteger)index isSameIndex:(BOOL)isSameIndex {
    
}

- (void)emojiKeyboard:(IMOEmojiKeyboard *)emojiKeyboard willShowEmojiPageAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - IMOMenuPickerDelegate

- (void)menuPicker:(IMOMenuPicker *)menuPicker didTapMenuIndex:(NSUInteger)index {
    NSString *title = [menuPicker titleAtIndex:index];
    if ([title isEqualToString:@"拍照"]) {
        
    } else {
        
    }
}

#pragma mark - Getter and setter

- (UIView *)topSeperatorView {
    if (!_topSeperatorView) {
        _topSeperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 0.5)];
        _topSeperatorView.backgroundColor = [UIColor seperatorColor];
        [self addSubview:_topSeperatorView];
    }
    return _topSeperatorView;
}

- (UITextView *)textView {
    if (!_textView) {
        _textView = [UITextView new];
        _textView.layer.borderColor = RGB(196, 196, 196).CGColor;
        _textView.textContainerInset = UIEdgeInsetsMake(9, 0, 0, 0);
        _textView.layer.borderWidth = 0.5;
        _textView.layer.cornerRadius = 5;
        _textView.font = [UIFont systemFontOfSize:16];
        _textView.delegate = self;
        [self addSubview:_textView];
    }
    return _textView;
}

- (UIButton *)voiceButton {
    if (!_voiceButton) {
        _voiceButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_voiceButton setImage:[UIImage imageNamed:@"chat_voice_normal"] forState:UIControlStateNormal];
        [_voiceButton setImage:[UIImage imageNamed:@"chat_keyboard_normal"] forState:UIControlStateSelected];
        [_voiceButton addTarget:self action:@selector(voiceButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_voiceButton];
    }
    return _voiceButton;
}

- (UIButton *)emojiButton {
    if (!_emojiButton) {
        _emojiButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_emojiButton setImage:[UIImage imageNamed:@"chat_express_normal"] forState:UIControlStateNormal];
        [_emojiButton setImage:[UIImage imageNamed:@"chat_keyboard_normal"] forState:UIControlStateSelected];
        [_emojiButton addTarget:self action:@selector(emojiButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_emojiButton];
    }
    return _emojiButton;
}

- (UIButton *)talkButton {
    if (!_talkButton) {
        _talkButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [_talkButton setTitle:@"按住 说话" forState:UIControlStateNormal];
        [_talkButton setTitle:@"松开 结束" forState:UIControlStateHighlighted];
        [_talkButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
        [_talkButton setTitleColor:RGB(80, 80, 80) forState:UIControlStateNormal];
        
        UIImage *resizedNormalImage = [[UIImage imageNamed:@"talk_button_normal"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch];
        [_talkButton setBackgroundImage:resizedNormalImage forState:UIControlStateNormal];
        
        UIImage *resizedHighlightImage = [[UIImage imageNamed:@"talk_button_highlight"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch];
        [_talkButton setBackgroundImage:resizedHighlightImage forState:UIControlStateHighlighted];
        
        [_talkButton addTarget:self action:@selector(talkButtonTouchUpOutside:) forControlEvents:UIControlEventTouchUpOutside];
        [_talkButton addTarget:self action:@selector(talkButtonTouchDown:) forControlEvents:UIControlEventTouchDown];
        [_talkButton addTarget:self action:@selector(talkButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [_talkButton addTarget:self action:@selector(talkButtonTouchDragExit:) forControlEvents:UIControlEventTouchDragExit];
        [_talkButton addTarget:self action:@selector(talkButtonTouchDragEnter:) forControlEvents:UIControlEventTouchDragEnter];
        [self addSubview:_talkButton];
        _talkButton.hidden = YES;
    }
    return _talkButton;
}

- (UIButton *)menuButton {
    if (!_menuButton) {
        _menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_menuButton setImage:[UIImage imageNamed:@"chat_add_normal"] forState:UIControlStateNormal];
        [_menuButton setImage:[UIImage imageNamed:@"chat_keyboard_normal"] forState:UIControlStateSelected];
        [_menuButton addTarget:self action:@selector(menuButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_menuButton];
    }
    return _menuButton;
}

- (IMOEmojiKeyboard *)emojiKeyboard {
    if (!_emojiKeyboard) {
        _emojiKeyboard = [[IMOEmojiKeyboard alloc] initWithFrame:CGRectMake(0, _inputBarHeight, CGRectGetWidth([UIScreen mainScreen].bounds), kEmojiKeyboardDefaultHeight)];
        _emojiKeyboard.dataSource = self;
        _emojiKeyboard.delegate = self;
    }
    return _emojiKeyboard;
}

- (IMOMenuPicker *)menuPicker {
    if (!_menuPicker) {
        _menuPicker = [[IMOMenuPicker alloc] initWithFrame:CGRectMake(0, _inputBarHeight, CGRectGetWidth([UIScreen mainScreen].bounds), _menuPickerHeight)];
        _menuPicker.delegate = self;

        NSMutableArray *items = [NSMutableArray array];
        NSArray *titles = @[@"照片", @"拍照", @"@提醒", @"位置", @"发任务", @"轻审批"];
        NSArray *normalImages = @[@"chat_ex_pic_normal", @"chat_ex_camera_normal", @"chat_ex_at_normal", @"chat_ex_location_normal", @"chat_ex_task_normal", @"chat_ex_approval_normal"];
        NSArray *highlightImages = @[@"chat_ex_pic_down", @"chat_ex_camera_down", @"chat_ex_at_down", @"chat_ex_location_down", @"chat_ex_task_down", @"chat_ex_approval_down"];
        
        for (int i = 0; i < titles.count; i++) {
            IMOPageItem *item = [IMOPageItem itemWithNormalName:normalImages[i]
                                                  highlightName:highlightImages[i]
                                                          title:titles[i]];
            [items addObject:item];
        }
        _menuPicker.pageItems = items;
    }
    return _menuPicker;
}

//- (UIImageView *)pullImageView {
//    if (!_pullImageView) {
//        _pullImageView = [[IMOPullImageView alloc] initWithFrame:CGRectMake(kPullImageOriginWidth-IMO_SCREEN_WIDTH, 0, IMO_SCREEN_WIDTH, kInputBarHeight)];
//        UIImage *image = [UIImage imageNamed:@"task_pull"];
//        image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(2, 1, 2, 9)];
//        _pullImageView.image = image;
//        _pullImageView.userInteractionEnabled = YES;
//        [self addSubview:_pullImageView];
//    }
//    return _pullImageView;
//}

#pragma mark - Memory manage

- (void)dealloc {
    _emojiKeyboard.dataSource = nil;
    _emojiKeyboard.delegate = nil;
    _menuPicker.delegate = nil;
    
    [self unregisterNotifications];
}

@end


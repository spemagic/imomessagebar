//
//  IMOEmojiKeyboardToolBar.m
//  IMOMessageBar
//
//  Created by zhanghao on 15/7/7.
//  Copyright (c) 2015年 zhanghao. All rights reserved.
//

#import "IMOEmojiKeyboardToolBar.h"
#import "IMOHorizontalLayout.h"
#import "IMOEmojiKeyboardToolBarCell.h"
#import "UIView+IMOAutoLayout.h"
#import "IMOMacros.h"

static NSString *const kEmojiKeyboardToolBarCellIdentifier = @"kEmojiKeyboardToolBarCellIdentifier";

@interface IMOEmojiKeyboardToolBar () <UICollectionViewDataSource, UICollectionViewDelegate>
{
    CGFloat _sendButtonWidth;
    NSIndexPath *_previousSelectedIndexPath;
}

@property (nonatomic, strong, readwrite) UICollectionView *collectionView;
@property (nonatomic, strong) UIView *seperatorView;
@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, strong) NSLayoutConstraint *sendButtonTrailingC;

@end

@implementation IMOEmojiKeyboardToolBar

#pragma mark - View lifecycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    _sendButtonWidth = 50;
    
    NSDictionary *metrics = @{ @"sendButtonWidth" : @(_sendButtonWidth) };
    NSDictionary *views = @{ @"collectionView" : self.collectionView,
                             @"seperatorView" : self.seperatorView,
                             @"sendButton" : self.sendButton };
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView][seperatorView(0.3)][sendButton(sendButtonWidth)]|" options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collectionView]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[seperatorView]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[sendButton]|" options:0 metrics:nil views:views]];
    
    self.sendButtonTrailingC = [self constraintForAttribute:NSLayoutAttributeTrailing firstItem:self secondItem:self.sendButton];
    
    [self.collectionView registerClass:[IMOEmojiKeyboardToolBarCell class] forCellWithReuseIdentifier:kEmojiKeyboardToolBarCellIdentifier];
}

#pragma mark - Public methods

- (void)setEnableSend:(BOOL)enableSend {
    _enableSend = enableSend;
    
    if (enableSend) {
        [_sendButton setTitleColor:RGB(255, 255, 255) forState:UIControlStateNormal];
        [_sendButton setBackgroundColor:RGB(12, 96, 254)];
    } else {
        [_sendButton setTitleColor:RGB(140, 140, 140) forState:UIControlStateNormal];
        [_sendButton setBackgroundColor:RGB(241, 241, 241)];
    }
}

- (void)showSendButton:(BOOL)show animated:(BOOL)animated {
    self.sendButtonTrailingC.constant = show ? 0 : -_sendButtonWidth;
    self.sendButton.layer.shadowOpacity = show ? 0.4 : 0;
    
    [UIView animateWithDuration:animated ? 0.2 : 0
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         if (animated) {
                             [self layoutIfNeeded];
                         }
                     }
                     completion:nil];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.emojiBarButtonNames.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    IMOEmojiKeyboardToolBarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kEmojiKeyboardToolBarCellIdentifier forIndexPath:indexPath];
    [cell fillCellWithEmojiBarButtonName:self.emojiBarButtonNames[indexPath.row]];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *indexPaths = [collectionView indexPathsForSelectedItems];
    if (indexPaths.count > 0) {
        _previousSelectedIndexPath = indexPaths.lastObject;
    }
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isSame = _previousSelectedIndexPath && _previousSelectedIndexPath.row == indexPath.row;
    if ([self.delegate respondsToSelector:@selector(emojiToolBar:didTapBarButtonIndex:isSameIndex:)]) {
        [self.delegate emojiToolBar:self didTapBarButtonIndex:indexPath.row isSameIndex:isSame];
    }
}

#pragma mark - Actions

- (void)sendButtonAction:(UIButton *)button {
    self.enableSend = NO;
    
    if ([self.delegate respondsToSelector:@selector(emojiToolBar:didTapSendButton:)]) {
        [self.delegate emojiToolBar:self didTapSendButton:button];
    }
}

#pragma mark - Getter and setter

- (void)setEmojiBarButtonNames:(NSArray *)emojiBarButtonNames {
    if (_emojiBarButtonNames != emojiBarButtonNames) {
        _emojiBarButtonNames = [emojiBarButtonNames copy];
    }
    
    if (_emojiBarButtonNames.count > 0) {
        [self.collectionView reloadData];
        [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    }
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        IMOHorizontalLayout *horizontalLayout = [[IMOHorizontalLayout alloc] initWithItemWidth:50];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:horizontalLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.bounces = YES;
        _collectionView.alwaysBounceHorizontal = YES;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollsToTop = NO;
        
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

- (UIView *)seperatorView {
    if (!_seperatorView) {
        _seperatorView = [UIView new];
        _seperatorView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.7];
        _seperatorView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_seperatorView];
    }
    return _seperatorView;
}

- (UIButton *)sendButton {
    if (!_sendButton) {
        _sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _sendButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_sendButton setTitle:@"发送" forState:UIControlStateNormal];
        [_sendButton.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
        [_sendButton addTarget:self action:@selector(sendButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        _sendButton.layer.shadowColor = RGB(140, 140, 140).CGColor;
        _sendButton.layer.shadowOffset = CGSizeMake(-1, 0);
        _sendButton.layer.shadowOpacity = 0.4;
        self.enableSend = NO;
        [self addSubview:_sendButton];
    }
    return _sendButton;
}

@end

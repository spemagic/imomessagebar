//
//  IMOHorizontalPageLayout.h
//  imoffice
//
//  Created by zhanghao on 15/7/6.
//  Copyright (c) 2015年 IMO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMOHorizontalPageLayout : UICollectionViewLayout

@property (nonatomic, assign, readonly) NSInteger numberOfRows;
@property (nonatomic, assign, readonly) NSInteger numberOfColumn;

@property (nonatomic, assign) NSInteger numberOfRowsInPortrait;
@property (nonatomic, assign) NSInteger numberOfRowsInLandscape;

@property (nonatomic, assign) NSInteger numberOfColumnInPortrait;
@property (nonatomic, assign) NSInteger numberOfColumnInLandscape;

@end
